/*
 * Copyright 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dokkatest.sampleAnnotation.samples

import androidx.annotations.Sampled
import androidx.notARealLibrary.Thingy1
import androidx.anotherFakeLibrary.FooFooFoo

@Sampled
fun FunctionContainingClassSample() {
    class CustomPainter() {

        val size: Pair<Float, Float>
            get() = Pair(300.0f, 300.0f)

        override fun toString(): String {
            return "ahahahaha"
        }

        val weAreUsingThingy1 = Thingy1()

    }
}

@Sampled
fun AnotherSampleInTheSameFile() {
    val weAreUsingFooFooFoo by lazy { FooFooFoo() }
}
