<html devsite="true">
  <head>
    <title>ViewModel</title>
{% setvar book_path %}/reference/androidx/_book.yaml{% endsetvar %}
{% include "_shared/_reference-head-tags.html" %}
  </head>
  <body>
    <div itemscope="" itemtype="http://developers.google.com/ReferenceObject">
      <meta itemprop="name" content="ViewModel">
      <meta itemprop="path" content="androidx.lifecycle">
      <meta itemprop="property" content="addCloseable(java.io.Closeable)">
      <meta itemprop="property" content="onCleared()">
      <meta itemprop="property" content="(androidx.lifecycle.ViewModel).getViewModelScope()">
      <meta itemprop="language" content="JAVA">
    </div>
    <div id="header-block">
      <div>
        <h1>ViewModel</h1>
      </div>
      <div id="metadata-info-block">
        <div id="source-link"><a href="https://cs.android.com/search?q=file:androidx/lifecycle/ViewModel.java+class:androidx.lifecycle.ViewModel&amp;ss=androidx/platform/frameworks/support" class="external">View Source</a></div>
      </div>
    </div>

{% setvar page_path %}androidx/lifecycle/ViewModel.html{% endsetvar %}
{% setvar can_switch %}1{% endsetvar %}
{% include "reference/_java_switcher2.md" %}
    <p>
      <pre>public abstract class <a href="/reference/androidx/lifecycle/ViewModel.html">ViewModel</a></pre>
    </p>
    <div class="devsite-table-wrapper"><devsite-expandable><span class="expand-control jd-sumtable-subclasses">Known direct subclasses
        <div class="showalways" id="subclasses-direct"><a href="/reference/androidx/lifecycle/AndroidViewModel.html">AndroidViewModel</a></div>
      </span>
      <div id="subclasses-direct-summary">
        <div class="devsite-table-wrapper">
          <table class="responsive">
            <colgroup>
              <col width="40%">
              <col>
            </colgroup>
            <tbody class="list">
              <tr>
                <td><code><a href="/reference/androidx/lifecycle/AndroidViewModel.html">AndroidViewModel</a></code></td>
                <td>
                  <p>Application context aware <code><a href="/reference/androidx/lifecycle/ViewModel.html">ViewModel</a></code>.</p>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
</devsite-expandable>    </div>
    <hr>
    <p>ViewModel is a class that is responsible for preparing and managing the data for an <code><a href="https://developer.android.com/reference/android/app/Activity.html">Activity</a></code> or a <code><a href="/reference/androidx/fragment/app/Fragment.html">Fragment</a></code>. It also handles the communication of the Activity / Fragment with the rest of the application (e.g. calling the business logic classes). </p>
    <p> A ViewModel is always created in association with a scope (a fragment or an activity) and will be retained as long as the scope is alive. E.g. if it is an Activity, until it is finished. </p>
    <p> In other words, this means that a ViewModel will not be destroyed if its owner is destroyed for a configuration change (e.g. rotation). The new owner instance just re-connects to the existing model. </p>
    <p> The purpose of the ViewModel is to acquire and keep the information that is necessary for an Activity or a Fragment. The Activity or the Fragment should be able to observe changes in the ViewModel. ViewModels usually expose this information via <code><a href="/reference/androidx/lifecycle/LiveData.html">LiveData</a></code> or Android Data Binding. You can also use any observability construct from your favorite framework. </p>
    <p> ViewModel's only responsibility is to manage the data for the UI. It <b>should never</b> access your view hierarchy or hold a reference back to the Activity or the Fragment. </p>
    <p> Typical usage from an Activity standpoint would be: </p>
    <pre class="prettyprint">public class UserActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_activity_layout);
        final UserModel viewModel = new ViewModelProvider(this).get(UserModel.class);
        viewModel.getUser().observe(this, new Observer&lt;User&gt;() {
            @Override
            public void onChanged(@Nullable User data) {
                // update ui.
            }
        });
        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 viewModel.doAction();
            }
        });
    }
}
</pre>
 ViewModel would be: 
    <pre class="prettyprint">public class UserModel extends ViewModel {
    private final MutableLiveData&lt;User&gt; userLiveData = new MutableLiveData&lt;&gt;();

    public LiveData&lt;User&gt; getUser() {
        return userLiveData;
    }

    public UserModel() {
        // trigger user load.
    }

    void doAction() {
        // depending on the action, do necessary business logic calls and update the
        // userLiveData.
    }
}
</pre>
    <p> ViewModels can also be used as a communication layer between different Fragments of an Activity. Each Fragment can acquire the ViewModel using the same key via their Activity. This allows communication between Fragments in a de-coupled fashion such that they never need to talk to the other Fragment directly. </p>
    <pre class="prettyprint">public class MyFragment extends Fragment {
    public void onStart() {
        UserModel userModel = new ViewModelProvider(requireActivity()).get(UserModel.class);
    }
}
</pre>
    <h2>Summary</h2>
    <div class="devsite-table-wrapper">
      <table class="responsive">
        <colgroup>
          <col width="40%">
          <col>
        </colgroup>
        <thead>
          <tr>
            <th colspan="100%"><h3>Public constructors</h3></th>
          </tr>
        </thead>
        <tbody class="list">
          <tr>
            <td>
              <div><code><a href="/reference/androidx/lifecycle/ViewModel.html#ViewModel()">ViewModel</a>()</code></div>
              <p>Construct a new ViewModel instance.</p>
            </td>
          </tr>
          <tr>
            <td>
              <div><code><a href="/reference/androidx/lifecycle/ViewModel.html#ViewModel(java.io.Closeable...)">ViewModel</a>(@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> Closeable[]&nbsp;closeables)</code></div>
              <p>Construct a new ViewModel instance.</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="devsite-table-wrapper">
      <table class="responsive">
        <colgroup>
          <col width="40%">
          <col>
        </colgroup>
        <thead>
          <tr>
            <th colspan="100%"><h3>Public methods</h3></th>
          </tr>
        </thead>
        <tbody class="list">
          <tr>
            <td><code>void</code></td>
            <td>
              <div><code><a href="/reference/androidx/lifecycle/ViewModel.html#addCloseable(java.io.Closeable)">addCloseable</a>(@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="https://developer.android.com/reference/java/io/Closeable.html">Closeable</a>&nbsp;closeable)</code></div>
              <p>Add a new <code><a href="https://developer.android.com/reference/java/io/Closeable.html">Closeable</a></code> object that will be closed directly before <code><a href="/reference/androidx/lifecycle/ViewModel.html#onCleared()">onCleared</a></code> is called.</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="devsite-table-wrapper">
      <table class="responsive">
        <colgroup>
          <col width="40%">
          <col>
        </colgroup>
        <thead>
          <tr>
            <th colspan="100%"><h3>Protected methods</h3></th>
          </tr>
        </thead>
        <tbody class="list">
          <tr>
            <td><code>void</code></td>
            <td>
              <div><code><a href="/reference/androidx/lifecycle/ViewModel.html#onCleared()">onCleared</a>()</code></div>
              <p>This method will be called when this ViewModel is no longer used and will be destroyed.</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="devsite-table-wrapper">
      <table class="responsive">
        <colgroup>
          <col width="40%">
          <col>
        </colgroup>
        <thead>
          <tr>
            <th colspan="100%"><h3>Extension functions</h3></th>
          </tr>
        </thead>
        <tbody class="list">
          <tr>
            <td><code>final @<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="https://kotlinlang.org/api/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines/-coroutine-scope/index.html">CoroutineScope</a></code></td>
            <td>
              <div><code><a href="/reference/androidx/lifecycle/ViewModelKt.html">ViewModelKt</a>.<a href="/reference/androidx/lifecycle/ViewModel.html#(androidx.lifecycle.ViewModel).getViewModelScope()">getViewModelScope</a>(@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/lifecycle/ViewModel.html">ViewModel</a>&nbsp;receiver)</code></div>
              <p><code><a href="https://kotlinlang.org/api/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines/-coroutine-scope/index.html">CoroutineScope</a></code> tied to this <code><a href="/reference/androidx/lifecycle/ViewModel.html">ViewModel</a></code>.</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="list">
      <h2>Public constructors</h2>
      <div class="api-item"><a name="ViewModel--"></a><a name="viewmodel"></a>
        <div class="api-name-block">
          <div>
            <h3 id="ViewModel()">ViewModel</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;<a href="/reference/androidx/lifecycle/ViewModel.html#ViewModel()">ViewModel</a>()</pre>
        <p>Construct a new ViewModel instance. </p>
        <p> You should <b>never</b> manually construct a ViewModel outside of a <code><a href="/reference/androidx/lifecycle/ViewModelProvider.Factory.html">ViewModelProvider.Factory</a></code>.</p>
      </div>
      <div class="api-item"><a name="ViewModel-java.io.Closeable...-"></a><a name="viewmodel"></a>
        <div class="api-name-block">
          <div>
            <h3 id="ViewModel(java.io.Closeable...)">ViewModel</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;<a href="/reference/androidx/lifecycle/ViewModel.html#ViewModel(java.io.Closeable...)">ViewModel</a>(@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> Closeable[]&nbsp;closeables)</pre>
        <p>Construct a new ViewModel instance. Any <code><a href="https://developer.android.com/reference/java/io/Closeable.html">Closeable</a></code> objects provided here will be closed directly before <code><a href="/reference/androidx/lifecycle/ViewModel.html#onCleared()">onCleared</a></code> is called. </p>
        <p> You should <b>never</b> manually construct a ViewModel outside of a <code><a href="/reference/androidx/lifecycle/ViewModelProvider.Factory.html">ViewModelProvider.Factory</a></code>.</p>
      </div>
    </div>
    <div class="list">
      <h2>Public methods</h2>
      <div class="api-item"><a name="addCloseable-java.io.Closeable-"></a><a name="addcloseable"></a>
        <div class="api-name-block">
          <div>
            <h3 id="addCloseable(java.io.Closeable)">addCloseable</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;void&nbsp;<a href="/reference/androidx/lifecycle/ViewModel.html#addCloseable(java.io.Closeable)">addCloseable</a>(@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="https://developer.android.com/reference/java/io/Closeable.html">Closeable</a>&nbsp;closeable)</pre>
        <p>Add a new <code><a href="https://developer.android.com/reference/java/io/Closeable.html">Closeable</a></code> object that will be closed directly before <code><a href="/reference/androidx/lifecycle/ViewModel.html#onCleared()">onCleared</a></code> is called.</p>
        <div class="devsite-table-wrapper">
          <table class="responsive">
            <colgroup>
              <col width="40%">
              <col>
            </colgroup>
            <thead>
              <tr>
                <th colspan="100%">Parameters</th>
              </tr>
            </thead>
            <tbody class="list">
              <tr>
                <td><code>@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="https://developer.android.com/reference/java/io/Closeable.html">Closeable</a>&nbsp;closeable</code></td>
                <td>
                  <p>The object that should be <code><a href="https://developer.android.com/reference/java/io/Closeable.html#close()">closed</a></code> directly before <code><a href="/reference/androidx/lifecycle/ViewModel.html#onCleared()">onCleared</a></code> is called.</p>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="list">
      <h2>Protected methods</h2>
      <div class="api-item"><a name="onCleared--"></a><a name="oncleared"></a>
        <div class="api-name-block">
          <div>
            <h3 id="onCleared()">onCleared</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">protected&nbsp;void&nbsp;<a href="/reference/androidx/lifecycle/ViewModel.html#onCleared()">onCleared</a>()</pre>
        <p>This method will be called when this ViewModel is no longer used and will be destroyed. </p>
        <p> It is useful when ViewModel observes some data and you need to clear this subscription to prevent a leak of this ViewModel.</p>
      </div>
    </div>
    <div class="list">
      <h2>Extension functions</h2>
      <div class="api-item"><a name="-androidx.lifecycle.ViewModel-.getViewModelScope--"></a><a name="getviewmodelscope"></a>
        <div class="api-name-block">
          <div>
            <h3 id="(androidx.lifecycle.ViewModel).getViewModelScope()">ViewModelKt.getViewModelScope</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;final&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="https://kotlinlang.org/api/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines/-coroutine-scope/index.html">CoroutineScope</a>&nbsp;<a href="/reference/androidx/lifecycle/ViewModelKt.html">ViewModelKt</a>.<a href="/reference/androidx/lifecycle/ViewModel.html#(androidx.lifecycle.ViewModel).getViewModelScope()">getViewModelScope</a>(@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/lifecycle/ViewModel.html">ViewModel</a>&nbsp;receiver)</pre>
        <p><code><a href="https://kotlinlang.org/api/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines/-coroutine-scope/index.html">CoroutineScope</a></code> tied to this <code><a href="/reference/androidx/lifecycle/ViewModel.html">ViewModel</a></code>. This scope will be canceled when ViewModel will be cleared, i.e <code><a href="/reference/androidx/lifecycle/ViewModel.html#onCleared()">ViewModel.onCleared</a></code> is called</p>
        <p>This scope is bound to <code><a href="https://kotlinlang.org/api/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines/-main-coroutine-dispatcher/immediate.html">Dispatchers.Main.immediate</a></code></p>
      </div>
    </div>
  </body>
</html>
