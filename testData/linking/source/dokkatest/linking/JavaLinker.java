/*
 * Copyright 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dokkatest.linking;

/**
 * Reference link {@link dokkatest.linking.KotlinEnum} should resolve <p>
 * sjuff sjuff {@link dokkatest.linking.KotlinEnum#ON_CREATE} should resolve, and so should {@link
 * dokkatest.linking.KotlinEnum#ON_CREATE}, and so should this one (making it multiline {@link
 * dokkatest.linking.KotlinEnum}. (It does not; b/195524451) <p>
 * sjujj sjujj {@link dokkatest.linking.JavaEnum#ON_DECEIT} should resolve
 */
public class JavaLinker {}
