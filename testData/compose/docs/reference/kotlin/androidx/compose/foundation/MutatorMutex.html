<html devsite="true">
  <head>
    <title>MutatorMutex</title>
{% setvar book_path %}/reference/kotlin/androidx/_book.yaml{% endsetvar %}
{% include "_shared/_reference-head-tags.html" %}
  </head>
  <body>
    <div itemscope="" itemtype="http://developers.google.com/ReferenceObject">
      <meta itemprop="name" content="MutatorMutex">
      <meta itemprop="path" content="androidx.compose.foundation">
      <meta itemprop="property" content="mutate(androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction0)">
      <meta itemprop="property" content="mutateWith(kotlin.Any,androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction1)">
      <meta itemprop="language" content="KOTLIN">
    </div>
    <div id="header-block">
      <div>
        <h1>MutatorMutex</h1>
      </div>
      <div id="metadata-info-block">
        <div id="source-link"><a href="https://cs.android.com/search?q=file:androidx/compose/foundation/MutatorMutex.kt+class:androidx.compose.foundation.MutatorMutex&amp;ss=androidx/platform/frameworks/support" class="external">View Source</a></div>
      </div>
    </div>

{% setvar page_path %}androidx/compose/foundation/MutatorMutex.html{% endsetvar %}
{% setvar can_switch %}1{% endsetvar %}
{% include "reference/_kotlin_switcher2.md" %}
    <devsite-select  id="platform" label="Select a platform"><select multiple="multiple"><option selected="selected" value="platform-Common/All">Common/All</option><option selected="selected" value="platform-Android/JVM">Android/JVM</option></select></devsite-select >
    <devsite-filter  select-el-container-id="platform">
      <div>
        <ul class="list" style="list-style: none; padding-left: 0">
          <li>
            <div class="kotlin-platform" data-title="Common/All">Cmn</div>
            <!--platform-Common/All-->
            <pre>class <a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html">MutatorMutex</a></pre>
          </li>
        </ul>
      </div>
    </devsite-filter >
    <hr>
    <p>Mutual exclusion for UI state mutation over time.</p>
    <p><code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutate(androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction0)">mutate</a></code> permits interruptible state mutation over time using a standard <code><a href="/reference/kotlin/androidx/compose/foundation/MutatePriority.html">MutatePriority</a></code>. A <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html">MutatorMutex</a></code> enforces that only a single writer can be active at a time for a particular state resource. Instead of queueing callers that would acquire the lock like a traditional Mutex, new attempts to <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutate(androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction0)">mutate</a></code> the guarded state will either cancel the current mutator or if the current mutator has a higher priority, the new caller will throw CancellationException.</p>
    <p><code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html">MutatorMutex</a></code> should be used for implementing hoisted state objects that many mutators may want to manipulate over time such that those mutators can coordinate with one another. The <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html">MutatorMutex</a></code> instance should be hidden as an implementation detail. For example:</p>
    <pre class="prettyprint lang-kotlin">
import androidx.compose.foundation.MutatorMutex
import androidx.compose.foundation.layout.Row
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.rememberCoroutineScope

@Stable
class ScrollState(position: Int = 0) {
    private var _position by mutableStateOf(position)
    var position: Int
        get() = _position.coerceAtMost(range)
        set(value) {
            _position = value.coerceIn(0, range)
        }

    private var _range by mutableStateOf(0)
    var range: Int
        get() = _range
        set(value) {
            _range = value.coerceAtLeast(0)
        }

    var isScrolling by mutableStateOf(false)
        private set

    private val mutatorMutex = MutatorMutex()

    /**
     * Only one caller to [scroll] can be in progress at a time.
     */
    suspend fun &lt;R&gt; scroll(
        block: suspend () -&gt; R
    ): R = mutatorMutex.mutate {
        isScrolling = true
        try {
            block()
        } finally {
            // MutatorMutex.mutate ensures mutual exclusion between blocks.
            // By setting back to false in the finally block inside mutate, we ensure that we
            // reset the state upon cancellation before the next block starts to run (if any).
            isScrolling = false
        }
    }
}

/**
 * Arbitrary animations can be defined as extensions using only public API
 */
suspend fun ScrollState.animateTo(target: Int) {
    scroll {
        animate(from = position, to = target) { newPosition -&gt;
            position = newPosition
        }
    }
}

/**
 * Presents two buttons for animating a scroll to the beginning or end of content.
 * Pressing one will cancel any current animation in progress.
 */
@Composable
fun ScrollControls(scrollState: ScrollState) {
    Row {
        val scope = rememberCoroutineScope()
        Button(onClick = { scope.launch { scrollState.animateTo(0) } }) {
            Text(&quot;Scroll to beginning&quot;)
        }
        Button(onClick = { scope.launch { scrollState.animateTo(scrollState.range) } }) {
            Text(&quot;Scroll to end&quot;)
        }
    }
}</pre>
    <h2>Summary</h2>
    <div class="devsite-table-wrapper">
      <devsite-filter  select-el-container-id="platform">
        <table class="fixed">
          <colgroup>
            <col width="93%">
            <col>
          </colgroup>
          <thead>
            <tr>
              <th colspan="100%"><h3>Public constructors</h3></th>
            </tr>
          </thead>
          <tbody class="list">
            <tr>
              <td>
                <div><code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#MutatorMutex()">MutatorMutex</a>()</code></div>
              </td>
              <td>
                <div class="kotlin-platform" data-title="Common/All">Cmn</div>
                <!--platform-Common/All--></td>
            </tr>
          </tbody>
        </table>
      </devsite-filter >
    </div>
    <div class="devsite-table-wrapper">
      <devsite-filter  select-el-container-id="platform">
        <table class="fixed">
          <colgroup>
            <col width="35%">
            <col width="58%">
            <col>
          </colgroup>
          <thead>
            <tr>
              <th colspan="100%"><h3>Public functions</h3></th>
            </tr>
          </thead>
          <tbody class="list">
            <tr>
              <td><code>suspend <a href="/reference/kotlin/[JVM root]/&lt;Error class: unknown class&gt;.html">&lt;Error class: unknown class&gt;</a></code></td>
              <td>
                <div><code>&lt;R&nbsp;:&nbsp;<a href="https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html">Any</a>?&gt; <a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutate(androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction0)">mutate</a>(priority:&nbsp;<a href="/reference/kotlin/androidx/compose/foundation/MutatePriority.html">MutatePriority</a>,&nbsp;block:&nbsp;suspend&nbsp;() <span style="white-space: nowrap;">-&gt;</span> R)</code></div>
                <p>Enforce that only a single caller may be active at a time.</p>
              </td>
              <td>
                <div class="kotlin-platform" data-title="Common/All">Cmn</div>
                <!--platform-Common/All--></td>
            </tr>
            <tr>
              <td><code>suspend <a href="/reference/kotlin/[JVM root]/&lt;Error class: unknown class&gt;.html">&lt;Error class: unknown class&gt;</a></code></td>
              <td>
                <div><code>&lt;T&nbsp;:&nbsp;<a href="https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html">Any</a>?,&nbsp;R&nbsp;:&nbsp;<a href="https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html">Any</a>?&gt; <a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutateWith(kotlin.Any,androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction1)">mutateWith</a>(<br>&nbsp;&nbsp;&nbsp;&nbsp;receiver:&nbsp;T,<br>&nbsp;&nbsp;&nbsp;&nbsp;priority:&nbsp;<a href="/reference/kotlin/androidx/compose/foundation/MutatePriority.html">MutatePriority</a>,<br>&nbsp;&nbsp;&nbsp;&nbsp;block:&nbsp;suspend&nbsp;T.() <span style="white-space: nowrap;">-&gt;</span> R<br>)</code></div>
                <p>Enforce that only a single caller may be active at a time.</p>
              </td>
              <td>
                <div class="kotlin-platform" data-title="Common/All">Cmn</div>
                <!--platform-Common/All--></td>
            </tr>
          </tbody>
        </table>
      </devsite-filter >
    </div>
    <devsite-filter  select-el-container-id="platform">
      <div class="list">
        <h2 data-title="platform-Common/All">Public constructors
          <!--platform-Common/All--></h2>
        <div class="api-item"><a name="MutatorMutex--"></a><a name="mutatormutex"></a>
          <div class="api-name-block">
            <div>
              <h3 id="MutatorMutex()">MutatorMutex</h3>
            </div>
            <div class="api-name-platform-and-metadata">
              <div class="api-name-platform-icons"><span class="kotlin-platform" data-title="Common/All">Cmn</span>
                <!--platform-Common/All--></div>
            </div>
          </div>
          <pre class="api-signature no-pretty-print"><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#MutatorMutex()">MutatorMutex</a>()</pre>
        </div>
      </div>
    </devsite-filter >
    <devsite-filter  select-el-container-id="platform">
      <div class="list">
        <h2 data-title="platform-Common/All">Public functions
          <!--platform-Common/All--></h2>
        <div class="api-item"><a name="mutate(androidx.compose.foundation.MutatePriority, kotlin.coroutines.SuspendFunction0)"></a><a name="mutate-androidx.compose.foundation.MutatePriority-kotlin.coroutines.SuspendFunction0-"></a><a name="mutate"></a>
          <div class="api-name-block">
            <div>
              <h3 id="mutate(androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction0)">mutate</h3>
            </div>
            <div class="api-name-platform-and-metadata">
              <div class="api-name-platform-icons"><span class="kotlin-platform" data-title="Common/All">Cmn</span>
                <!--platform-Common/All--></div>
            </div>
          </div>
          <pre class="api-signature no-pretty-print">suspend&nbsp;fun&nbsp;&lt;R&nbsp;:&nbsp;<a href="https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html">Any</a>?&gt; <a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutate(androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction0)">mutate</a>(<br>&nbsp;&nbsp;&nbsp;&nbsp;priority:&nbsp;<a href="/reference/kotlin/androidx/compose/foundation/MutatePriority.html">MutatePriority</a> = MutatePriority.Default,<br>&nbsp;&nbsp;&nbsp;&nbsp;block:&nbsp;suspend&nbsp;() <span style="white-space: nowrap;">-&gt;</span> R<br>):&nbsp;<a href="/reference/kotlin/[JVM root]/&lt;Error class: unknown class&gt;.html">&lt;Error class: unknown class&gt;</a></pre>
          <p>Enforce that only a single caller may be active at a time.</p>
          <p>If <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutate(androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction0)">mutate</a></code> is called while another call to <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutate(androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction0)">mutate</a></code> or <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutateWith(kotlin.Any,androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction1)">mutateWith</a></code> is in progress, their <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutate(androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction0)">priority</a></code> values are compared. If the new caller has a <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutate(androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction0)">priority</a></code> equal to or higher than the call in progress, the call in progress will be cancelled, throwing CancellationException and the new caller's <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutate(androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction0)">block</a></code> will be invoked. If the call in progress had a higher <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutate(androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction0)">priority</a></code> than the new caller, the new caller will throw CancellationException without invoking <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutate(androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction0)">block</a></code>.</p>
          <div class="devsite-table-wrapper">
            <table class="responsive">
              <colgroup>
                <col width="40%">
                <col>
              </colgroup>
              <thead>
                <tr>
                  <th colspan="100%">Parameters</th>
                </tr>
              </thead>
              <tbody class="list">
                <tr>
                  <td><code>priority:&nbsp;<a href="/reference/kotlin/androidx/compose/foundation/MutatePriority.html">MutatePriority</a> = MutatePriority.Default</code></td>
                  <td>
                    <p>the priority of this mutation; <code><a href="/reference/kotlin/androidx/compose/foundation/MutatePriority.html#Default">MutatePriority.Default</a></code> by default. Higher priority mutations will interrupt lower priority mutations.</p>
                  </td>
                </tr>
                <tr>
                  <td><code>block:&nbsp;suspend&nbsp;() <span style="white-space: nowrap;">-&gt;</span> R</code></td>
                  <td>
                    <p>mutation code to run mutually exclusive with any other call to <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutate(androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction0)">mutate</a></code> or <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutateWith(kotlin.Any,androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction1)">mutateWith</a></code>.</p>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="api-item"><a name="mutateWith(kotlin.Any, androidx.compose.foundation.MutatePriority, kotlin.coroutines.SuspendFunction1)"></a><a name="mutateWith-kotlin.Any-androidx.compose.foundation.MutatePriority-kotlin.coroutines.SuspendFunction1-"></a><a name="mutatewith"></a>
          <div class="api-name-block">
            <div>
              <h3 id="mutateWith(kotlin.Any,androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction1)">mutateWith</h3>
            </div>
            <div class="api-name-platform-and-metadata">
              <div class="api-name-platform-icons"><span class="kotlin-platform" data-title="Common/All">Cmn</span>
                <!--platform-Common/All--></div>
            </div>
          </div>
          <pre class="api-signature no-pretty-print">suspend&nbsp;fun&nbsp;&lt;T&nbsp;:&nbsp;<a href="https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html">Any</a>?,&nbsp;R&nbsp;:&nbsp;<a href="https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html">Any</a>?&gt; <a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutateWith(kotlin.Any,androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction1)">mutateWith</a>(<br>&nbsp;&nbsp;&nbsp;&nbsp;receiver:&nbsp;T,<br>&nbsp;&nbsp;&nbsp;&nbsp;priority:&nbsp;<a href="/reference/kotlin/androidx/compose/foundation/MutatePriority.html">MutatePriority</a> = MutatePriority.Default,<br>&nbsp;&nbsp;&nbsp;&nbsp;block:&nbsp;suspend&nbsp;T.() <span style="white-space: nowrap;">-&gt;</span> R<br>):&nbsp;<a href="/reference/kotlin/[JVM root]/&lt;Error class: unknown class&gt;.html">&lt;Error class: unknown class&gt;</a></pre>
          <p>Enforce that only a single caller may be active at a time.</p>
          <p>If <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutateWith(kotlin.Any,androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction1)">mutateWith</a></code> is called while another call to <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutate(androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction0)">mutate</a></code> or <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutateWith(kotlin.Any,androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction1)">mutateWith</a></code> is in progress, their <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutateWith(kotlin.Any,androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction1)">priority</a></code> values are compared. If the new caller has a <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutateWith(kotlin.Any,androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction1)">priority</a></code> equal to or higher than the call in progress, the call in progress will be cancelled, throwing CancellationException and the new caller's <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutateWith(kotlin.Any,androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction1)">block</a></code> will be invoked. If the call in progress had a higher <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutateWith(kotlin.Any,androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction1)">priority</a></code> than the new caller, the new caller will throw CancellationException without invoking <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutateWith(kotlin.Any,androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction1)">block</a></code>.</p>
          <p>This variant of <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutate(androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction0)">mutate</a></code> calls its <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutateWith(kotlin.Any,androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction1)">block</a></code> with a <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutateWith(kotlin.Any,androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction1)">receiver</a></code>, removing the need to create an additional capturing lambda to invoke it with a receiver object. This can be used to expose a mutable scope to the provided <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutateWith(kotlin.Any,androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction1)">block</a></code> while leaving the rest of the state object read-only. For example:</p>
          <pre class="prettyprint lang-kotlin">
import androidx.compose.foundation.MutatorMutex
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.Stable
import androidx.compose.runtime.mutableStateOf

@Stable
class ScrollState(position: Int = 0) {
    private var _position = mutableStateOf(position)
    val position: Int by _position

    private val mutatorMutex = MutatorMutex()

    /**
     * Only [block] in a call to [scroll] may change the value of [position].
     */
    suspend fun &lt;R&gt; scroll(
        block: suspend MutableState&lt;Int&gt;.() -&gt; R
    ): R = mutatorMutex.mutateWith(_position, block = block)
}</pre>
          <div class="devsite-table-wrapper">
            <table class="responsive">
              <colgroup>
                <col width="40%">
                <col>
              </colgroup>
              <thead>
                <tr>
                  <th colspan="100%">Parameters</th>
                </tr>
              </thead>
              <tbody class="list">
                <tr>
                  <td><code>receiver:&nbsp;T</code></td>
                  <td>
                    <p>the receiver <code>this</code> that <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutateWith(kotlin.Any,androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction1)">block</a></code> will be called with</p>
                  </td>
                </tr>
                <tr>
                  <td><code>priority:&nbsp;<a href="/reference/kotlin/androidx/compose/foundation/MutatePriority.html">MutatePriority</a> = MutatePriority.Default</code></td>
                  <td>
                    <p>the priority of this mutation; <code><a href="/reference/kotlin/androidx/compose/foundation/MutatePriority.html#Default">MutatePriority.Default</a></code> by default. Higher priority mutations will interrupt lower priority mutations.</p>
                  </td>
                </tr>
                <tr>
                  <td><code>block:&nbsp;suspend&nbsp;T.() <span style="white-space: nowrap;">-&gt;</span> R</code></td>
                  <td>
                    <p>mutation code to run mutually exclusive with any other call to <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutate(androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction0)">mutate</a></code> or <code><a href="/reference/kotlin/androidx/compose/foundation/MutatorMutex.html#mutateWith(kotlin.Any,androidx.compose.foundation.MutatePriority,kotlin.coroutines.SuspendFunction1)">mutateWith</a></code>.</p>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </devsite-filter >
  </body>
</html>
