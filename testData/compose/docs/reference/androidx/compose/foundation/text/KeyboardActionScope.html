<html devsite="true">
  <head>
    <title>KeyboardActionScope</title>
{% setvar book_path %}/reference/androidx/_book.yaml{% endsetvar %}
{% include "_shared/_reference-head-tags.html" %}
  </head>
  <body>
    <div itemscope="" itemtype="http://developers.google.com/ReferenceObject">
      <meta itemprop="name" content="KeyboardActionScope">
      <meta itemprop="path" content="androidx.compose.foundation.text">
      <meta itemprop="property" content="defaultKeyboardAction(androidx.compose.ui.text.input.ImeAction)">
      <meta itemprop="language" content="JAVA">
    </div>
    <div id="header-block">
      <div>
        <h1>KeyboardActionScope</h1>
      </div>
      <div id="metadata-info-block">
        <div id="source-link"><a href="https://cs.android.com/search?q=file:androidx/compose/foundation/text/KeyboardActions.kt+class:androidx.compose.foundation.text.KeyboardActionScope&amp;ss=androidx/platform/frameworks/support" class="external">View Source</a></div>
      </div>
    </div>

{% setvar page_path %}androidx/compose/foundation/text/KeyboardActionScope.html{% endsetvar %}
{% setvar can_switch %}1{% endsetvar %}
{% include "reference/_java_switcher2.md" %}
    <p>
      <pre>public interface <a href="/reference/androidx/compose/foundation/text/KeyboardActionScope.html">KeyboardActionScope</a></pre>
    </p>
    <hr>
    <p>This scope can be used to execute the default action implementation.</p>
    <h2>Summary</h2>
    <div class="devsite-table-wrapper">
      <table class="responsive">
        <colgroup>
          <col width="40%">
          <col>
        </colgroup>
        <thead>
          <tr>
            <th colspan="100%"><h3>Public methods</h3></th>
          </tr>
        </thead>
        <tbody class="list">
          <tr>
            <td><code>abstract void</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/foundation/text/KeyboardActionScope.html#defaultKeyboardAction(androidx.compose.ui.text.input.ImeAction)">defaultKeyboardAction</a>(@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/ui/text/input/ImeAction.html">ImeAction</a>&nbsp;imeAction)</code></div>
              <p>Runs the default implementation for the specified <code><a href="/reference/androidx/compose/ui/text/input/ImeAction.html">action</a></code>.</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="list">
      <h2>Public methods</h2>
      <div class="api-item"><a name="defaultKeyboardAction-androidx.compose.ui.text.input.ImeAction-"></a><a name="defaultkeyboardaction"></a>
        <div class="api-name-block">
          <div>
            <h3 id="defaultKeyboardAction(androidx.compose.ui.text.input.ImeAction)">defaultKeyboardAction</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">abstract&nbsp;void&nbsp;<a href="/reference/androidx/compose/foundation/text/KeyboardActionScope.html#defaultKeyboardAction(androidx.compose.ui.text.input.ImeAction)">defaultKeyboardAction</a>(@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/ui/text/input/ImeAction.html">ImeAction</a>&nbsp;imeAction)</pre>
        <p>Runs the default implementation for the specified <code><a href="/reference/androidx/compose/ui/text/input/ImeAction.html">action</a></code>.</p>
      </div>
    </div>
  </body>
</html>
