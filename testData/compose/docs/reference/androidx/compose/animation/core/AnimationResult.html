<html devsite="true">
  <head>
    <title>AnimationResult</title>
{% setvar book_path %}/reference/androidx/_book.yaml{% endsetvar %}
{% include "_shared/_reference-head-tags.html" %}
  </head>
  <body>
    <div itemscope="" itemtype="http://developers.google.com/ReferenceObject">
      <meta itemprop="name" content="AnimationResult">
      <meta itemprop="path" content="androidx.compose.animation.core">
      <meta itemprop="property" content="getEndReason()">
      <meta itemprop="property" content="getEndState()">
      <meta itemprop="property" content="toString()">
      <meta itemprop="language" content="JAVA">
    </div>
    <div id="header-block">
      <div>
        <h1>AnimationResult</h1>
      </div>
      <div id="metadata-info-block">
        <div id="source-link"><a href="https://cs.android.com/search?q=file:androidx/compose/animation/core/Animatable.kt+class:androidx.compose.animation.core.AnimationResult&amp;ss=androidx/platform/frameworks/support" class="external">View Source</a></div>
      </div>
    </div>

{% setvar page_path %}androidx/compose/animation/core/AnimationResult.html{% endsetvar %}
{% setvar can_switch %}1{% endsetvar %}
{% include "reference/_java_switcher2.md" %}
    <p>
      <pre>public final class <a href="/reference/androidx/compose/animation/core/AnimationResult.html">AnimationResult</a>&lt;T&nbsp;extends&nbsp;<a href="https://developer.android.com/reference/java/lang/Object.html">Object</a>,&nbsp;V&nbsp;extends&nbsp;<a href="/reference/androidx/compose/animation/core/AnimationVector.html">AnimationVector</a>&gt;</pre>
    </p>
    <hr>
    <p>AnimationResult contains information about an animation at the end of the animation. <code><a href="/reference/androidx/compose/animation/core/AnimationResult.html#endState()">endState</a></code> captures the value/velocity/frame time, etc of the animation at its last frame. It can be useful for starting another animation to continue the velocity from the previously interrupted animation. <code><a href="/reference/androidx/compose/animation/core/AnimationResult.html#endReason()">endReason</a></code> describes why the animation ended, it could be either of the following:</p>
    <ul>
      <li>
        <p><code><a href="/reference/androidx/compose/animation/core/AnimationEndReason.html#Finished">Finished</a></code>, when the animation finishes successfully without any interruption</p>
      </li>
      <li>
        <p><code><a href="/reference/androidx/compose/animation/core/AnimationEndReason.html#BoundReached">BoundReached</a></code> If the animation reaches the either <code><a href="/reference/androidx/compose/animation/core/Animatable.html#lowerBound()">lowerBound</a></code> or     <code><a href="/reference/androidx/compose/animation/core/Animatable.html#upperBound()">upperBound</a></code> in any dimension, the animation will end with     <code><a href="/reference/androidx/compose/animation/core/AnimationEndReason.html#BoundReached">BoundReached</a></code> being the end reason.</p>
      </li>
    </ul>
    <pre class="prettyprint lang-kotlin">
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.AnimationEndReason
import androidx.compose.animation.core.exponentialDecay
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size

suspend fun CoroutineScope.animateBouncingOffBounds(
    animatable: Animatable&lt;Offset, *&gt;,
    flingVelocity: Offset,
    parentSize: Size
) {
    launch {
        var startVelocity = flingVelocity
        // Set bounds for the animation, so that when it reaches bounds it will stop
        // immediately. We can then inspect the returned `AnimationResult` and decide whether
        // we should start another animation.
        animatable.updateBounds(Offset(0f, 0f), Offset(parentSize.width, parentSize.height))
        do {
            val result = animatable.animateDecay(startVelocity, exponentialDecay())
            // Copy out the end velocity of the previous animation.
            startVelocity = result.endState.velocity

            // Negate the velocity for the dimension that hits the bounds, to create a
            // bouncing off the bounds effect.
            with(animatable) {
                if (value.x == upperBound?.x || value.x == lowerBound?.x) {
                    // x dimension hits bounds
                    startVelocity = startVelocity.copy(x = -startVelocity.x)
                }
                if (value.y == upperBound?.y || value.y == lowerBound?.y) {
                    // y dimension hits bounds
                    startVelocity = startVelocity.copy(y = -startVelocity.y)
                }
            }
            // Repeat the animation until the animation ends for reasons other than hitting
            // bounds, e.g. if `stop()` is called, or preempted by another animation.
        } while (result.endReason == AnimationEndReason.BoundReached)
    }
}</pre>
    <h2>Summary</h2>
    <div class="devsite-table-wrapper">
      <table class="responsive">
        <colgroup>
          <col width="40%">
          <col>
        </colgroup>
        <thead>
          <tr>
            <th colspan="100%"><h3>Public constructors</h3></th>
          </tr>
        </thead>
        <tbody class="list">
          <tr>
            <td>
              <div><code>&lt;T&nbsp;extends&nbsp;<a href="https://developer.android.com/reference/java/lang/Object.html">Object</a>,&nbsp;V&nbsp;extends&nbsp;<a href="/reference/androidx/compose/animation/core/AnimationVector.html">AnimationVector</a>&gt; <a href="/reference/androidx/compose/animation/core/AnimationResult.html#AnimationResult(androidx.compose.animation.core.AnimationState,androidx.compose.animation.core.AnimationEndReason)">AnimationResult</a>(<br>&nbsp;&nbsp;&nbsp;&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/AnimationState.html">AnimationState</a>&lt;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T,&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> V&gt;&nbsp;endState,<br>&nbsp;&nbsp;&nbsp;&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/AnimationEndReason.html">AnimationEndReason</a>&nbsp;endReason<br>)</code></div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="devsite-table-wrapper">
      <table class="responsive">
        <colgroup>
          <col width="40%">
          <col>
        </colgroup>
        <thead>
          <tr>
            <th colspan="100%"><h3>Public methods</h3></th>
          </tr>
        </thead>
        <tbody class="list">
          <tr>
            <td><code>final @<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/AnimationEndReason.html">AnimationEndReason</a></code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/animation/core/AnimationResult.html#getEndReason()">getEndReason</a>()</code></div>
              <p>The reason why the animation has ended.</p>
            </td>
          </tr>
          <tr>
            <td><code>final @<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/AnimationState.html">AnimationState</a>&lt;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T,&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> V&gt;</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/animation/core/AnimationResult.html#getEndState()">getEndState</a>()</code></div>
              <p>The state of the animation in its last frame before it's canceled or reset.</p>
            </td>
          </tr>
          <tr>
            <td><code>@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="https://developer.android.com/reference/java/lang/String.html">String</a></code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/animation/core/AnimationResult.html#toString()">toString</a>()</code></div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="list">
      <h2>Public constructors</h2>
      <div class="api-item"><a name="AnimationResult(androidx.compose.animation.core.AnimationState, androidx.compose.animation.core.AnimationEndReason)"></a><a name="AnimationResult-androidx.compose.animation.core.AnimationState-androidx.compose.animation.core.AnimationEndReason-"></a><a name="animationresult"></a>
        <div class="api-name-block">
          <div>
            <h3 id="AnimationResult(androidx.compose.animation.core.AnimationState,androidx.compose.animation.core.AnimationEndReason)">AnimationResult</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;&lt;T&nbsp;extends&nbsp;<a href="https://developer.android.com/reference/java/lang/Object.html">Object</a>,&nbsp;V&nbsp;extends&nbsp;<a href="/reference/androidx/compose/animation/core/AnimationVector.html">AnimationVector</a>&gt; <a href="/reference/androidx/compose/animation/core/AnimationResult.html#AnimationResult(androidx.compose.animation.core.AnimationState,androidx.compose.animation.core.AnimationEndReason)">AnimationResult</a>(<br>&nbsp;&nbsp;&nbsp;&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/AnimationState.html">AnimationState</a>&lt;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T,&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> V&gt;&nbsp;endState,<br>&nbsp;&nbsp;&nbsp;&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/AnimationEndReason.html">AnimationEndReason</a>&nbsp;endReason<br>)</pre>
      </div>
    </div>
    <div class="list">
      <h2>Public methods</h2>
      <div class="api-item"><a name="getEndReason--"></a><a name="getendreason"></a>
        <div class="api-name-block">
          <div>
            <h3 id="getEndReason()">getEndReason</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;final&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/AnimationEndReason.html">AnimationEndReason</a>&nbsp;<a href="/reference/androidx/compose/animation/core/AnimationResult.html#getEndReason()">getEndReason</a>()</pre>
        <p>The reason why the animation has ended. Could be either of the following:</p>
        <ul>
          <li>
            <p><code><a href="/reference/androidx/compose/animation/core/AnimationEndReason.html#Finished">Finished</a></code>, when the animation finishes successfully without any interruption</p>
          </li>
          <li>
            <p><code><a href="/reference/androidx/compose/animation/core/AnimationEndReason.html#BoundReached">BoundReached</a></code> If the animation reaches the either <code><a href="/reference/androidx/compose/animation/core/Animatable.html#lowerBound()">lowerBound</a></code> or     <code><a href="/reference/androidx/compose/animation/core/Animatable.html#upperBound()">upperBound</a></code> in any dimension, the animation will end with     <code><a href="/reference/androidx/compose/animation/core/AnimationEndReason.html#BoundReached">BoundReached</a></code> being the end reason.</p>
          </li>
        </ul>
      </div>
      <div class="api-item"><a name="getEndState--"></a><a name="getendstate"></a>
        <div class="api-name-block">
          <div>
            <h3 id="getEndState()">getEndState</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;final&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/AnimationState.html">AnimationState</a>&lt;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T,&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> V&gt;&nbsp;<a href="/reference/androidx/compose/animation/core/AnimationResult.html#getEndState()">getEndState</a>()</pre>
        <p>The state of the animation in its last frame before it's canceled or reset. This captures the animation value/velocity/frame time, etc at the point of interruption, or before the velocity is reset when the animation finishes successfully.</p>
      </div>
      <div class="api-item"><a name="toString--"></a><a name="tostring"></a>
        <div class="api-name-block">
          <div>
            <h3 id="toString()">toString</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="https://developer.android.com/reference/java/lang/String.html">String</a>&nbsp;<a href="/reference/androidx/compose/animation/core/AnimationResult.html#toString()">toString</a>()</pre>
      </div>
    </div>
  </body>
</html>
