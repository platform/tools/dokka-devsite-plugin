<html devsite="true">
  <head>
    <title>InfiniteRepeatableSpec</title>
{% setvar book_path %}/reference/androidx/_book.yaml{% endsetvar %}
{% include "_shared/_reference-head-tags.html" %}
  </head>
  <body>
    <div itemscope="" itemtype="http://developers.google.com/ReferenceObject">
      <meta itemprop="name" content="InfiniteRepeatableSpec">
      <meta itemprop="path" content="androidx.compose.animation.core">
      <meta itemprop="property" content="equals(kotlin.Any)">
      <meta itemprop="property" content="getAnimation()">
      <meta itemprop="property" content="getInitialStartOffset()">
      <meta itemprop="property" content="getRepeatMode()">
      <meta itemprop="property" content="hashCode()">
      <meta itemprop="property" content="vectorize(androidx.compose.animation.core.TwoWayConverter)">
      <meta itemprop="language" content="JAVA">
    </div>
    <div id="header-block">
      <div>
        <h1>InfiniteRepeatableSpec</h1>
      </div>
      <div id="metadata-info-block">
        <div id="source-link"><a href="https://cs.android.com/search?q=file:androidx/compose/animation/core/AnimationSpec.kt+class:androidx.compose.animation.core.InfiniteRepeatableSpec&amp;ss=androidx/platform/frameworks/support" class="external">View Source</a></div>
      </div>
    </div>

{% setvar page_path %}androidx/compose/animation/core/InfiniteRepeatableSpec.html{% endsetvar %}
{% setvar can_switch %}1{% endsetvar %}
{% include "reference/_java_switcher2.md" %}
    <p>
      <pre>public final class <a href="/reference/androidx/compose/animation/core/InfiniteRepeatableSpec.html">InfiniteRepeatableSpec</a>&lt;T&nbsp;extends&nbsp;<a href="https://developer.android.com/reference/java/lang/Object.html">Object</a>&gt; implements <a href="/reference/androidx/compose/animation/core/AnimationSpec.html">AnimationSpec</a></pre>
    </p>
    <hr>
    <p><code><a href="/reference/androidx/compose/animation/core/InfiniteRepeatableSpec.html">InfiniteRepeatableSpec</a></code> repeats the provided <code><a href="/reference/androidx/compose/animation/core/InfiniteRepeatableSpec.html#animation()">animation</a></code> infinite amount of times. It will never naturally finish. This means the animation will only be stopped via some form of manual cancellation. When used with transition or other animation composables, the infinite animations will stop when the composable is removed from the compose tree.</p>
    <p>For non-infinite repeating animations, consider <code><a href="/reference/androidx/compose/animation/core/RepeatableSpec.html">RepeatableSpec</a></code>.</p>
    <p><code><a href="/reference/androidx/compose/animation/core/InfiniteRepeatableSpec.html#initialStartOffset()">initialStartOffset</a></code> can be used to either delay the start of the animation or to fast forward the animation to a given play time. This start offset will <b>not</b> be repeated, whereas the delay in the <code><a href="/reference/androidx/compose/animation/core/InfiniteRepeatableSpec.html#animation()">animation</a></code> (if any) will be repeated. By default, the amount of offset is 0.</p>
    <pre class="prettyprint lang-kotlin">
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.StartOffset
import androidx.compose.animation.core.StartOffsetType
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.unit.dp

// This is an infinite progress indicator with 3 pulsing dots that grow and shrink.
@Composable
fun Dot(scale: State&lt;Float&gt;) {
    Box(
        Modifier.padding(5.dp).size(20.dp).graphicsLayer {
            scaleX = scale.value
            scaleY = scale.value
        }.background(Color.Gray, shape = CircleShape)
    )
}

val infiniteTransition = rememberInfiniteTransition()
val scale1 = infiniteTransition.animateFloat(
    0.2f,
    1f,
    // No offset for the 1st animation
    infiniteRepeatable(tween(600), RepeatMode.Reverse)
)
val scale2 = infiniteTransition.animateFloat(
    0.2f,
    1f,
    infiniteRepeatable(
        tween(600), RepeatMode.Reverse,
        // Offsets the 2nd animation by starting from 150ms of the animation
        // This offset will not be repeated.
        initialStartOffset = StartOffset(offsetMillis = 150, StartOffsetType.FastForward)
    )
)
val scale3 = infiniteTransition.animateFloat(
    0.2f,
    1f,
    infiniteRepeatable(
        tween(600), RepeatMode.Reverse,
        // Offsets the 3rd animation by starting from 300ms of the animation. This
        // offset will be not repeated.
        initialStartOffset = StartOffset(offsetMillis = 300, StartOffsetType.FastForward)
    )
)
Row {
    Dot(scale1)
    Dot(scale2)
    Dot(scale3)
}</pre>
    <div class="devsite-table-wrapper">
      <table class="responsive">
        <colgroup>
          <col width="40%">
          <col>
        </colgroup>
        <thead>
          <tr>
            <th colspan="100%">See also</th>
          </tr>
        </thead>
        <tbody class="list">
          <tr>
            <td><code><a href="/reference/androidx/compose/animation/core/package-summary.html#infiniteRepeatable(androidx.compose.animation.core.DurationBasedAnimationSpec,androidx.compose.animation.core.RepeatMode,androidx.compose.animation.core.StartOffset)">infiniteRepeatable</a></code></td>
            <td></td>
          </tr>
        </tbody>
      </table>
    </div>
    <h2>Summary</h2>
    <div class="devsite-table-wrapper">
      <table class="responsive">
        <colgroup>
          <col width="40%">
          <col>
        </colgroup>
        <thead>
          <tr>
            <th colspan="100%"><h3>Public constructors</h3></th>
          </tr>
        </thead>
        <tbody class="list">
          <tr>
            <td>
              <div><code>&lt;T&nbsp;extends&nbsp;<a href="https://developer.android.com/reference/java/lang/Object.html">Object</a>&gt; <a href="/reference/androidx/compose/animation/core/InfiniteRepeatableSpec.html#InfiniteRepeatableSpec(androidx.compose.animation.core.DurationBasedAnimationSpec,androidx.compose.animation.core.RepeatMode,androidx.compose.animation.core.StartOffset)">InfiniteRepeatableSpec</a>(<br>&nbsp;&nbsp;&nbsp;&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/DurationBasedAnimationSpec.html">DurationBasedAnimationSpec</a>&lt;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T&gt;&nbsp;animation,<br>&nbsp;&nbsp;&nbsp;&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/RepeatMode.html">RepeatMode</a>&nbsp;repeatMode,<br>&nbsp;&nbsp;&nbsp;&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/StartOffset.html">StartOffset</a>&nbsp;initialStartOffset<br>)</code></div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="devsite-table-wrapper">
      <table class="responsive">
        <colgroup>
          <col width="40%">
          <col>
        </colgroup>
        <thead>
          <tr>
            <th colspan="100%"><h3>Public methods</h3></th>
          </tr>
        </thead>
        <tbody class="list">
          <tr>
            <td><code>boolean</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/animation/core/InfiniteRepeatableSpec.html#equals(kotlin.Any)">equals</a>(<a href="https://developer.android.com/reference/java/lang/Object.html">Object</a>&nbsp;other)</code></div>
            </td>
          </tr>
          <tr>
            <td><code>final @<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/DurationBasedAnimationSpec.html">DurationBasedAnimationSpec</a>&lt;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T&gt;</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/animation/core/InfiniteRepeatableSpec.html#getAnimation()">getAnimation</a>()</code></div>
              <p>the <code><a href="/reference/androidx/compose/animation/core/AnimationSpec.html">AnimationSpec</a></code> to be repeated</p>
            </td>
          </tr>
          <tr>
            <td><code>final @<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/StartOffset.html">StartOffset</a></code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/animation/core/InfiniteRepeatableSpec.html#getInitialStartOffset()">getInitialStartOffset</a>()</code></div>
              <p>offsets the start of the animation</p>
            </td>
          </tr>
          <tr>
            <td><code>final @<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/RepeatMode.html">RepeatMode</a></code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/animation/core/InfiniteRepeatableSpec.html#getRepeatMode()">getRepeatMode</a>()</code></div>
              <p>whether animation should repeat by starting from the beginning (i.e.     <code><a href="/reference/androidx/compose/animation/core/RepeatMode.html#Restart">RepeatMode.Restart</a></code>) or from the end (i.e. <code><a href="/reference/androidx/compose/animation/core/RepeatMode.html#Reverse">RepeatMode.Reverse</a></code>)</p>
            </td>
          </tr>
          <tr>
            <td><code>int</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/animation/core/InfiniteRepeatableSpec.html#hashCode()">hashCode</a>()</code></div>
            </td>
          </tr>
          <tr>
            <td><code>@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/VectorizedAnimationSpec.html">VectorizedAnimationSpec</a>&lt;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> V&gt;</code></td>
            <td>
              <div><code>&lt;V&nbsp;extends&nbsp;<a href="/reference/androidx/compose/animation/core/AnimationVector.html">AnimationVector</a>&gt; <a href="/reference/androidx/compose/animation/core/InfiniteRepeatableSpec.html#vectorize(androidx.compose.animation.core.TwoWayConverter)">vectorize</a>(<br>&nbsp;&nbsp;&nbsp;&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/TwoWayConverter.html">TwoWayConverter</a>&lt;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T,&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> V&gt;&nbsp;converter<br>)</code></div>
              <p>Creates a <code><a href="/reference/androidx/compose/animation/core/VectorizedAnimationSpec.html">VectorizedAnimationSpec</a></code> with the given <code><a href="/reference/androidx/compose/animation/core/TwoWayConverter.html">TwoWayConverter</a></code>.</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="list">
      <h2>Public constructors</h2>
      <div class="api-item"><a name="InfiniteRepeatableSpec(androidx.compose.animation.core.DurationBasedAnimationSpec, androidx.compose.animation.core.RepeatMode, androidx.compose.animation.core.StartOffset)"></a><a name="InfiniteRepeatableSpec-androidx.compose.animation.core.DurationBasedAnimationSpec-androidx.compose.animation.core.RepeatMode-androidx.compose.animation.core.StartOffset-"></a><a name="infiniterepeatablespec"></a>
        <div class="api-name-block">
          <div>
            <h3 id="InfiniteRepeatableSpec(androidx.compose.animation.core.DurationBasedAnimationSpec,androidx.compose.animation.core.RepeatMode,androidx.compose.animation.core.StartOffset)">InfiniteRepeatableSpec</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;&lt;T&nbsp;extends&nbsp;<a href="https://developer.android.com/reference/java/lang/Object.html">Object</a>&gt; <a href="/reference/androidx/compose/animation/core/InfiniteRepeatableSpec.html#InfiniteRepeatableSpec(androidx.compose.animation.core.DurationBasedAnimationSpec,androidx.compose.animation.core.RepeatMode,androidx.compose.animation.core.StartOffset)">InfiniteRepeatableSpec</a>(<br>&nbsp;&nbsp;&nbsp;&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/DurationBasedAnimationSpec.html">DurationBasedAnimationSpec</a>&lt;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T&gt;&nbsp;animation,<br>&nbsp;&nbsp;&nbsp;&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/RepeatMode.html">RepeatMode</a>&nbsp;repeatMode,<br>&nbsp;&nbsp;&nbsp;&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/StartOffset.html">StartOffset</a>&nbsp;initialStartOffset<br>)</pre>
        <div class="devsite-table-wrapper">
          <table class="responsive">
            <colgroup>
              <col width="40%">
              <col>
            </colgroup>
            <thead>
              <tr>
                <th colspan="100%">Parameters</th>
              </tr>
            </thead>
            <tbody class="list">
              <tr>
                <td><code>@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/DurationBasedAnimationSpec.html">DurationBasedAnimationSpec</a>&lt;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T&gt;&nbsp;animation</code></td>
                <td>
                  <p>the <code><a href="/reference/androidx/compose/animation/core/AnimationSpec.html">AnimationSpec</a></code> to be repeated</p>
                </td>
              </tr>
              <tr>
                <td><code>@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/RepeatMode.html">RepeatMode</a>&nbsp;repeatMode</code></td>
                <td>
                  <p>whether animation should repeat by starting from the beginning (i.e.     <code><a href="/reference/androidx/compose/animation/core/RepeatMode.html#Restart">RepeatMode.Restart</a></code>) or from the end (i.e. <code><a href="/reference/androidx/compose/animation/core/RepeatMode.html#Reverse">RepeatMode.Reverse</a></code>)</p>
                </td>
              </tr>
              <tr>
                <td><code>@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/StartOffset.html">StartOffset</a>&nbsp;initialStartOffset</code></td>
                <td>
                  <p>offsets the start of the animation</p>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="list">
      <h2>Public methods</h2>
      <div class="api-item"><a name="equals-kotlin.Any-"></a><a name="equals"></a>
        <div class="api-name-block">
          <div>
            <h3 id="equals(kotlin.Any)">equals</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;boolean&nbsp;<a href="/reference/androidx/compose/animation/core/InfiniteRepeatableSpec.html#equals(kotlin.Any)">equals</a>(<a href="https://developer.android.com/reference/java/lang/Object.html">Object</a>&nbsp;other)</pre>
      </div>
      <div class="api-item"><a name="getAnimation--"></a><a name="getanimation"></a>
        <div class="api-name-block">
          <div>
            <h3 id="getAnimation()">getAnimation</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;final&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/DurationBasedAnimationSpec.html">DurationBasedAnimationSpec</a>&lt;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T&gt;&nbsp;<a href="/reference/androidx/compose/animation/core/InfiniteRepeatableSpec.html#getAnimation()">getAnimation</a>()</pre>
        <p>the <code><a href="/reference/androidx/compose/animation/core/AnimationSpec.html">AnimationSpec</a></code> to be repeated</p>
      </div>
      <div class="api-item"><a name="getInitialStartOffset--"></a><a name="getinitialstartoffset"></a>
        <div class="api-name-block">
          <div>
            <h3 id="getInitialStartOffset()">getInitialStartOffset</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;final&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/StartOffset.html">StartOffset</a>&nbsp;<a href="/reference/androidx/compose/animation/core/InfiniteRepeatableSpec.html#getInitialStartOffset()">getInitialStartOffset</a>()</pre>
        <p>offsets the start of the animation</p>
      </div>
      <div class="api-item"><a name="getRepeatMode--"></a><a name="getrepeatmode"></a>
        <div class="api-name-block">
          <div>
            <h3 id="getRepeatMode()">getRepeatMode</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;final&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/RepeatMode.html">RepeatMode</a>&nbsp;<a href="/reference/androidx/compose/animation/core/InfiniteRepeatableSpec.html#getRepeatMode()">getRepeatMode</a>()</pre>
        <p>whether animation should repeat by starting from the beginning (i.e.     <code><a href="/reference/androidx/compose/animation/core/RepeatMode.html#Restart">RepeatMode.Restart</a></code>) or from the end (i.e. <code><a href="/reference/androidx/compose/animation/core/RepeatMode.html#Reverse">RepeatMode.Reverse</a></code>)</p>
      </div>
      <div class="api-item"><a name="hashCode--"></a><a name="hashcode"></a>
        <div class="api-name-block">
          <div>
            <h3 id="hashCode()">hashCode</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;int&nbsp;<a href="/reference/androidx/compose/animation/core/InfiniteRepeatableSpec.html#hashCode()">hashCode</a>()</pre>
      </div>
      <div class="api-item"><a name="vectorize-androidx.compose.animation.core.TwoWayConverter-"></a><a name="vectorize"></a>
        <div class="api-name-block">
          <div>
            <h3 id="vectorize(androidx.compose.animation.core.TwoWayConverter)">vectorize</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/VectorizedAnimationSpec.html">VectorizedAnimationSpec</a>&lt;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> V&gt;&nbsp;&lt;V&nbsp;extends&nbsp;<a href="/reference/androidx/compose/animation/core/AnimationVector.html">AnimationVector</a>&gt; <a href="/reference/androidx/compose/animation/core/InfiniteRepeatableSpec.html#vectorize(androidx.compose.animation.core.TwoWayConverter)">vectorize</a>(<br>&nbsp;&nbsp;&nbsp;&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/TwoWayConverter.html">TwoWayConverter</a>&lt;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T,&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> V&gt;&nbsp;converter<br>)</pre>
        <p>Creates a <code><a href="/reference/androidx/compose/animation/core/VectorizedAnimationSpec.html">VectorizedAnimationSpec</a></code> with the given <code><a href="/reference/androidx/compose/animation/core/TwoWayConverter.html">TwoWayConverter</a></code>.</p>
        <p>The underlying animation system operates on <code><a href="/reference/androidx/compose/animation/core/AnimationVector.html">AnimationVector</a></code>s. <code><a href="/reference/androidx/compose/animation/core/InfiniteRepeatableSpec.html">T</a></code> will be converted to <code><a href="/reference/androidx/compose/animation/core/AnimationVector.html">AnimationVector</a></code> to animate. <code><a href="/reference/androidx/compose/animation/core/VectorizedAnimationSpec.html">VectorizedAnimationSpec</a></code> describes how the converted <code><a href="/reference/androidx/compose/animation/core/AnimationVector.html">AnimationVector</a></code> should be animated. E.g. The animation could simply interpolate between the start and end values (i.e.<code><a href="/reference/androidx/compose/animation/core/TweenSpec.html">TweenSpec</a></code>), or apply spring physics to produce the motion (i.e. <code><a href="/reference/androidx/compose/animation/core/SpringSpec.html">SpringSpec</a></code>), etc)</p>
        <div class="devsite-table-wrapper">
          <table class="responsive">
            <colgroup>
              <col width="40%">
              <col>
            </colgroup>
            <thead>
              <tr>
                <th colspan="100%">Parameters</th>
              </tr>
            </thead>
            <tbody class="list">
              <tr>
                <td><code>@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/animation/core/TwoWayConverter.html">TwoWayConverter</a>&lt;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T,&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> V&gt;&nbsp;converter</code></td>
                <td>
                  <p>converts the type <code><a href="/reference/androidx/compose/animation/core/InfiniteRepeatableSpec.html">T</a></code> from and to <code><a href="/reference/androidx/compose/animation/core/AnimationVector.html">AnimationVector</a></code> type</p>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </body>
</html>
