<html devsite="true">
  <head>
    <title>LayoutAwareModifierNode</title>
{% setvar book_path %}/reference/androidx/_book.yaml{% endsetvar %}
{% include "_shared/_reference-head-tags.html" %}
  </head>
  <body>
    <div itemscope="" itemtype="http://developers.google.com/ReferenceObject">
      <meta itemprop="name" content="LayoutAwareModifierNode">
      <meta itemprop="path" content="androidx.compose.ui.node">
      <meta itemprop="property" content="onPlaced(androidx.compose.ui.layout.LayoutCoordinates)">
      <meta itemprop="property" content="onRemeasured(androidx.compose.ui.unit.IntSize)">
      <meta itemprop="language" content="JAVA">
    </div>
    <div id="header-block">
      <div>
        <h1>LayoutAwareModifierNode</h1>
      </div>
      <div id="metadata-info-block">
        <div id="source-link"><a href="https://cs.android.com/search?q=file:androidx/compose/ui/node/LayoutAwareModifierNode.kt+class:androidx.compose.ui.node.LayoutAwareModifierNode&amp;ss=androidx/platform/frameworks/support" class="external">View Source</a></div>
      </div>
    </div>

{% setvar page_path %}androidx/compose/ui/node/LayoutAwareModifierNode.html{% endsetvar %}
{% setvar can_switch %}1{% endsetvar %}
{% include "reference/_java_switcher2.md" %}
    <p>
      <pre>public interface <a href="/reference/androidx/compose/ui/node/LayoutAwareModifierNode.html">LayoutAwareModifierNode</a> extends <a href="/reference/androidx/compose/ui/node/DelegatableNode.html">DelegatableNode</a></pre>
    </p>
    <hr>
    <p>A <code><a href="/reference/androidx/compose/ui/Modifier.Node.html">androidx.compose.ui.Modifier.Node</a></code> which receives various callbacks in response to local changes in layout.</p>
    <p>This is the <code><a href="/reference/androidx/compose/ui/Modifier.Node.html">androidx.compose.ui.Modifier.Node</a></code> equivalent of <code><a href="/reference/androidx/compose/ui/layout/OnRemeasuredModifier.html">androidx.compose.ui.layout.OnRemeasuredModifier</a></code> and <code><a href="/reference/androidx/compose/ui/layout/OnPlacedModifier.html">androidx.compose.ui.layout.OnPlacedModifier</a></code></p>
    <p>Example usage:</p>
    <pre class="prettyprint lang-kotlin">
import androidx.compose.material.Text
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.onSizeChanged

// Use onSizeChanged() for diagnostics. Use Layout or SubcomposeLayout if you want
// to use the size of one component to affect the size of another component.
Text(
    &quot;Hello $name&quot;,
    Modifier.onSizeChanged { size -&gt;
        println(&quot;The size of the Text in pixels is $size&quot;)
    }
)</pre>
    <pre class="prettyprint lang-kotlin">
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.AnimationVector2D
import androidx.compose.animation.core.Spring.StiffnessMediumLow
import androidx.compose.animation.core.VectorConverter
import androidx.compose.animation.core.spring
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onPlaced
import androidx.compose.ui.layout.positionInParent
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.round

@OptIn(ExperimentalComposeUiApi::class)
fun Modifier.animatePlacement(): Modifier = composed {
    val scope = rememberCoroutineScope()
    var targetOffset by remember { mutableStateOf(IntOffset.Zero) }
    var animatable by remember {
        mutableStateOf&lt;Animatable&lt;IntOffset, AnimationVector2D&gt;?&gt;(null)
    }
    this.onPlaced {
        // Calculate the position in the parent layout
        targetOffset = it.positionInParent().round()
    }.offset {
        // Animate to the new target offset when alignment changes.
        val anim = animatable ?: Animatable(targetOffset, IntOffset.VectorConverter)
            .also { animatable = it }
        if (anim.targetValue != targetOffset) {
            scope.launch {
                anim.animateTo(targetOffset, spring(stiffness = StiffnessMediumLow))
            }
        }
        // Offset the child in the opposite direction to the targetOffset, and slowly catch
        // up to zero offset via an animation to achieve an overall animated movement.
        animatable?.let { it.value - targetOffset } ?: IntOffset.Zero
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun AnimatedChildAlignment(alignment: Alignment) {
    Box(
        Modifier.fillMaxSize().padding(4.dp).border(1.dp, Color.Red)
    ) {
        Box(
            modifier = Modifier.animatePlacement().align(alignment).size(100.dp)
                .background(Color.Red)
        )
    }
}</pre>
    <pre class="prettyprint lang-kotlin">
import androidx.compose.ui.Modifier
import androidx.compose.ui.node.LayoutAwareModifierNode
import androidx.compose.ui.node.ModifierNodeElement
import androidx.compose.ui.platform.InspectorInfo
import androidx.compose.ui.unit.IntSize

class SizeLoggerNode(var id: String) : LayoutAwareModifierNode, Modifier.Node() {
    override fun onRemeasured(size: IntSize) {
        println(&quot;The size of $id was $size&quot;)
    }
}

data class LogSizeElement(val id: String) : ModifierNodeElement&lt;SizeLoggerNode&gt;() {
    override fun create(): SizeLoggerNode = SizeLoggerNode(id)
    override fun update(node: SizeLoggerNode) {
        node.id = id
    }
    override fun InspectorInfo.inspectableProperties() {
        name = &quot;logSize&quot;
        properties[&quot;id&quot;] = id
    }
}

fun Modifier.logSize(id: String) = this then LogSizeElement(id)</pre>
    <h2>Summary</h2>
    <div class="devsite-table-wrapper">
      <table class="responsive">
        <colgroup>
          <col width="40%">
          <col>
        </colgroup>
        <thead>
          <tr>
            <th colspan="100%"><h3>Public methods</h3></th>
          </tr>
        </thead>
        <tbody class="list">
          <tr>
            <td><code>default void</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/ui/node/LayoutAwareModifierNode.html#onPlaced(androidx.compose.ui.layout.LayoutCoordinates)">onPlaced</a>(@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/ui/layout/LayoutCoordinates.html">LayoutCoordinates</a>&nbsp;coordinates)</code></div>
              <p><code><a href="/reference/androidx/compose/ui/node/LayoutAwareModifierNode.html#onPlaced(androidx.compose.ui.layout.LayoutCoordinates)">onPlaced</a></code> is called after the parent <code><a href="/reference/androidx/compose/ui/layout/LayoutModifier.html">LayoutModifier</a></code> and parent layout has been placed and before child <code><a href="/reference/androidx/compose/ui/layout/LayoutModifier.html">LayoutModifier</a></code> is placed.</p>
            </td>
          </tr>
          <tr>
            <td><code>default void</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/ui/node/LayoutAwareModifierNode.html#onRemeasured(androidx.compose.ui.unit.IntSize)">onRemeasured</a>(@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/ui/unit/IntSize.html">IntSize</a>&nbsp;size)</code></div>
              <p>This method is called when the layout content is remeasured.</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="devsite-table-wrapper">
      <table class="responsive" id="inhmethods">
        <thead>
          <tr>
            <th colspan="100%"><h3>Inherited methods</h3></th>
          </tr>
        </thead>
        <tbody class="list">
          <tr>
            <td><devsite-expandable><span class="expand-control">From <a href="/reference/androidx/compose/ui/node/DelegatableNode.html">androidx.compose.ui.node.DelegatableNode</a></span>
              <div class="devsite-table-wrapper">
                <table class="responsive">
                  <colgroup>
                    <col width="40%">
                    <col>
                  </colgroup>
                  <tbody class="list">
                    <tr>
                      <td><code>abstract @<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/ui/Modifier.Node.html">Modifier.Node</a></code></td>
                      <td>
                        <div><code><a href="/reference/androidx/compose/ui/node/DelegatableNode.html#getNode()">getNode</a>()</code></div>
                        <p>A reference of the <code><a href="/reference/androidx/compose/ui/Modifier.Node.html">Modifier.Node</a></code> that holds this node's position in the node hierarchy.</p>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
</devsite-expandable>            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="list">
      <h2>Public methods</h2>
      <div class="api-item"><a name="onPlaced-androidx.compose.ui.layout.LayoutCoordinates-"></a><a name="onplaced"></a>
        <div class="api-name-block">
          <div>
            <h3 id="onPlaced(androidx.compose.ui.layout.LayoutCoordinates)">onPlaced</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">default&nbsp;void&nbsp;<a href="/reference/androidx/compose/ui/node/LayoutAwareModifierNode.html#onPlaced(androidx.compose.ui.layout.LayoutCoordinates)">onPlaced</a>(@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/ui/layout/LayoutCoordinates.html">LayoutCoordinates</a>&nbsp;coordinates)</pre>
        <p><code><a href="/reference/androidx/compose/ui/node/LayoutAwareModifierNode.html#onPlaced(androidx.compose.ui.layout.LayoutCoordinates)">onPlaced</a></code> is called after the parent <code><a href="/reference/androidx/compose/ui/layout/LayoutModifier.html">LayoutModifier</a></code> and parent layout has been placed and before child <code><a href="/reference/androidx/compose/ui/layout/LayoutModifier.html">LayoutModifier</a></code> is placed. This allows child <code><a href="/reference/androidx/compose/ui/layout/LayoutModifier.html">LayoutModifier</a></code> to adjust its own placement based on where the parent is.</p>
      </div>
      <div class="api-item"><a name="onRemeasured-androidx.compose.ui.unit.IntSize-"></a><a name="onremeasured"></a>
        <div class="api-name-block">
          <div>
            <h3 id="onRemeasured(androidx.compose.ui.unit.IntSize)">onRemeasured</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">default&nbsp;void&nbsp;<a href="/reference/androidx/compose/ui/node/LayoutAwareModifierNode.html#onRemeasured(androidx.compose.ui.unit.IntSize)">onRemeasured</a>(@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/ui/unit/IntSize.html">IntSize</a>&nbsp;size)</pre>
        <p>This method is called when the layout content is remeasured. The most common usage is <code><a href="/reference/androidx/compose/ui/layout/package-summary.html#(androidx.compose.ui.Modifier).onSizeChanged(kotlin.Function1)">onSizeChanged</a></code>.</p>
      </div>
    </div>
  </body>
</html>
