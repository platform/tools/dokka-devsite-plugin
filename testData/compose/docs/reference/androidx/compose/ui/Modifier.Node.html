<html devsite="true">
  <head>
    <title>Modifier.Node</title>
{% setvar book_path %}/reference/androidx/_book.yaml{% endsetvar %}
{% include "_shared/_reference-head-tags.html" %}
  </head>
  <body>
    <div itemscope="" itemtype="http://developers.google.com/ReferenceObject">
      <meta itemprop="name" content="Modifier.Node">
      <meta itemprop="path" content="androidx.compose.ui">
      <meta itemprop="property" content="getCoroutineScope()">
      <meta itemprop="property" content="getNode()">
      <meta itemprop="property" content="getShouldAutoInvalidate()">
      <meta itemprop="property" content="getIsAttached()">
      <meta itemprop="property" content="onAttach()">
      <meta itemprop="property" content="onDetach()">
      <meta itemprop="property" content="onReset()">
      <meta itemprop="property" content="sideEffect(kotlin.Function0)">
      <meta itemprop="language" content="JAVA">
    </div>
    <div id="header-block">
      <div>
        <h1>Modifier.Node</h1>
      </div>
      <div id="metadata-info-block">
        <div id="source-link"><a href="https://cs.android.com/search?q=file:androidx/compose/ui/Modifier.kt+class:androidx.compose.ui.Modifier.Node&amp;ss=androidx/platform/frameworks/support" class="external">View Source</a></div>
      </div>
    </div>

{% setvar page_path %}androidx/compose/ui/Modifier.Node.html{% endsetvar %}
{% setvar can_switch %}1{% endsetvar %}
{% include "reference/_java_switcher2.md" %}
    <p>
      <pre>public abstract class <a href="/reference/androidx/compose/ui/Modifier.Node.html">Modifier.Node</a> implements <a href="/reference/androidx/compose/ui/node/DelegatableNode.html">DelegatableNode</a></pre>
    </p>
    <div class="devsite-table-wrapper"><devsite-expandable><span class="expand-control jd-sumtable-subclasses">Known direct subclasses
        <div class="showalways" id="subclasses-direct"><a href="/reference/androidx/compose/ui/node/DelegatingNode.html">DelegatingNode</a></div>
      </span>
      <div id="subclasses-direct-summary">
        <div class="devsite-table-wrapper">
          <table class="responsive">
            <colgroup>
              <col width="40%">
              <col>
            </colgroup>
            <tbody class="list">
              <tr>
                <td><code><a href="/reference/androidx/compose/ui/node/DelegatingNode.html">DelegatingNode</a></code></td>
                <td>
                  <p>A <code><a href="/reference/androidx/compose/ui/Modifier.Node.html">Modifier.Node</a></code> which is able to delegate work to other <code><a href="/reference/androidx/compose/ui/Modifier.Node.html">Modifier.Node</a></code> instances.</p>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
</devsite-expandable>    </div>
    <hr>
    <p>The longer-lived object that is created for each <code><a href="/reference/androidx/compose/ui/Modifier.Element.html">Modifier.Element</a></code> applied to a <code><a href="/reference/androidx/compose/ui/layout/package-summary.html#Layout(kotlin.Function0,androidx.compose.ui.Modifier,androidx.compose.ui.layout.MeasurePolicy)">androidx.compose.ui.layout.Layout</a></code>. Most <code><a href="/reference/androidx/compose/ui/Modifier.Node.html">Modifier.Node</a></code> implementations will have a corresponding &quot;Modifier Factory&quot; extension method on Modifier that will allow them to be used indirectly, without ever implementing a <code><a href="/reference/androidx/compose/ui/Modifier.Node.html">Modifier.Node</a></code> subclass directly. In some cases it may be useful to define a custom <code><a href="/reference/androidx/compose/ui/Modifier.Node.html">Modifier.Node</a></code> subclass in order to efficiently implement some collection of behaviors that requires maintaining state over time and over many recompositions where the various provided Modifier factories are not sufficient.</p>
    <p>When a <code><a href="/reference/androidx/compose/ui/Modifier.html">Modifier</a></code> is set on a <code><a href="/reference/androidx/compose/ui/layout/package-summary.html#Layout(kotlin.Function0,androidx.compose.ui.Modifier,androidx.compose.ui.layout.MeasurePolicy)">androidx.compose.ui.layout.Layout</a></code>, each <code><a href="/reference/androidx/compose/ui/Modifier.Element.html">Modifier.Element</a></code> contained in that linked list will result in a corresponding <code><a href="/reference/androidx/compose/ui/Modifier.Node.html">Modifier.Node</a></code> instance in a matching linked list of <code><a href="/reference/androidx/compose/ui/Modifier.Node.html">Modifier.Node</a></code>s that the <code><a href="/reference/androidx/compose/ui/layout/package-summary.html#Layout(kotlin.Function0,androidx.compose.ui.Modifier,androidx.compose.ui.layout.MeasurePolicy)">androidx.compose.ui.layout.Layout</a></code> will hold on to. As subsequent <code><a href="/reference/androidx/compose/ui/Modifier.html">Modifier</a></code> chains get set on the <code><a href="/reference/androidx/compose/ui/layout/package-summary.html#Layout(kotlin.Function0,androidx.compose.ui.Modifier,androidx.compose.ui.layout.MeasurePolicy)">androidx.compose.ui.layout.Layout</a></code>, the linked list of <code><a href="/reference/androidx/compose/ui/Modifier.Node.html">Modifier.Node</a></code>s will be diffed and updated as appropriate, even though the <code><a href="/reference/androidx/compose/ui/Modifier.html">Modifier</a></code> instance might be completely new. As a result, the lifetime of a <code><a href="/reference/androidx/compose/ui/Modifier.Node.html">Modifier.Node</a></code> is the intersection of the lifetime of the <code><a href="/reference/androidx/compose/ui/layout/package-summary.html#Layout(kotlin.Function0,androidx.compose.ui.Modifier,androidx.compose.ui.layout.MeasurePolicy)">androidx.compose.ui.layout.Layout</a></code> that it lives on and a corresponding <code><a href="/reference/androidx/compose/ui/Modifier.Element.html">Modifier.Element</a></code> being present in the <code><a href="/reference/androidx/compose/ui/layout/package-summary.html#Layout(kotlin.Function0,androidx.compose.ui.Modifier,androidx.compose.ui.layout.MeasurePolicy)">androidx.compose.ui.layout.Layout</a></code>'s <code><a href="/reference/androidx/compose/ui/Modifier.html">Modifier</a></code>.</p>
    <p>If one creates a subclass of <code><a href="/reference/androidx/compose/ui/Modifier.Node.html">Modifier.Node</a></code>, it is expected that it will implement one or more interfaces that interact with the various Compose UI subsystems. To use the <code><a href="/reference/androidx/compose/ui/Modifier.Node.html">Modifier.Node</a></code> subclass, it is expected that it will be instantiated by adding a <code><a href="/reference/androidx/compose/ui/node/ModifierNodeElement.html">androidx.compose.ui.node.ModifierNodeElement</a></code> to a <code><a href="/reference/androidx/compose/ui/Modifier.html">Modifier</a></code> chain.</p>
    <div class="devsite-table-wrapper">
      <table class="responsive">
        <colgroup>
          <col width="40%">
          <col>
        </colgroup>
        <thead>
          <tr>
            <th colspan="100%">See also</th>
          </tr>
        </thead>
        <tbody class="list">
          <tr>
            <td><code><a href="/reference/androidx/compose/ui/node/ModifierNodeElement.html">ModifierNodeElement</a></code></td>
            <td></td>
          </tr>
          <tr>
            <td><code><a href="/reference/androidx/compose/ui/node/CompositionLocalConsumerModifierNode.html">CompositionLocalConsumerModifierNode</a></code></td>
            <td></td>
          </tr>
          <tr>
            <td><code><a href="/reference/androidx/compose/ui/node/DelegatableNode.html">DelegatableNode</a></code></td>
            <td></td>
          </tr>
          <tr>
            <td><code><a href="/reference/androidx/compose/ui/node/DelegatingNode.html">DelegatingNode</a></code></td>
            <td></td>
          </tr>
          <tr>
            <td><code><a href="/reference/androidx/compose/ui/node/LayoutModifierNode.html">LayoutModifierNode</a></code></td>
            <td></td>
          </tr>
          <tr>
            <td><code><a href="/reference/androidx/compose/ui/node/DrawModifierNode.html">DrawModifierNode</a></code></td>
            <td></td>
          </tr>
          <tr>
            <td><code><a href="/reference/androidx/compose/ui/node/SemanticsModifierNode.html">SemanticsModifierNode</a></code></td>
            <td></td>
          </tr>
          <tr>
            <td><code><a href="/reference/androidx/compose/ui/node/PointerInputModifierNode.html">PointerInputModifierNode</a></code></td>
            <td></td>
          </tr>
          <tr>
            <td><code><a href="/reference/androidx/compose/ui/modifier/ModifierLocalModifierNode.html">ModifierLocalModifierNode</a></code></td>
            <td></td>
          </tr>
          <tr>
            <td><code><a href="/reference/androidx/compose/ui/node/ParentDataModifierNode.html">ParentDataModifierNode</a></code></td>
            <td></td>
          </tr>
          <tr>
            <td><code><a href="/reference/androidx/compose/ui/node/LayoutAwareModifierNode.html">LayoutAwareModifierNode</a></code></td>
            <td></td>
          </tr>
          <tr>
            <td><code><a href="/reference/androidx/compose/ui/node/GlobalPositionAwareModifierNode.html">GlobalPositionAwareModifierNode</a></code></td>
            <td></td>
          </tr>
          <tr>
            <td><code><a href="/reference/androidx/compose/ui/node/IntermediateLayoutModifierNode.html">IntermediateLayoutModifierNode</a></code></td>
            <td></td>
          </tr>
        </tbody>
      </table>
    </div>
    <h2>Summary</h2>
    <div class="devsite-table-wrapper">
      <table class="responsive">
        <colgroup>
          <col width="40%">
          <col>
        </colgroup>
        <thead>
          <tr>
            <th colspan="100%"><h3>Public constructors</h3></th>
          </tr>
        </thead>
        <tbody class="list">
          <tr>
            <td>
              <div><code><a href="/reference/androidx/compose/ui/Modifier.Node.html#Node()">Node</a>()</code></div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="devsite-table-wrapper">
      <table class="responsive">
        <colgroup>
          <col width="40%">
          <col>
        </colgroup>
        <thead>
          <tr>
            <th colspan="100%"><h3>Public methods</h3></th>
          </tr>
        </thead>
        <tbody class="list">
          <tr>
            <td><code>final @<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/[JVM root]/&lt;Error class: unknown class&gt;.html">&lt;Error class: unknown class&gt;</a></code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/ui/Modifier.Node.html#getCoroutineScope()">getCoroutineScope</a>()</code></div>
              <p>A CoroutineScope that can be used to launch tasks that should run while the node is attached.</p>
            </td>
          </tr>
          <tr>
            <td><code>final @<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/ui/Modifier.Node.html">Modifier.Node</a></code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/ui/Modifier.Node.html#getNode()">getNode</a>()</code></div>
              <p>A reference of the <code><a href="/reference/androidx/compose/ui/Modifier.Node.html">Modifier.Node</a></code> that holds this node's position in the node hierarchy.</p>
            </td>
          </tr>
          <tr>
            <td><code>boolean</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/ui/Modifier.Node.html#getShouldAutoInvalidate()">getShouldAutoInvalidate</a>()</code></div>
              <p>If this property returns <code>true</code>, then nodes will be automatically invalidated after the modifier update completes (For example, if the returned Node is a <code><a href="/reference/androidx/compose/ui/node/DrawModifierNode.html">DrawModifierNode</a></code>, its <code><a href="/reference/androidx/compose/ui/node/package-summary.html#(androidx.compose.ui.node.DrawModifierNode).invalidateDraw()">DrawModifierNode.invalidateDraw</a></code> function will be invoked automatically as part of auto invalidation).</p>
            </td>
          </tr>
          <tr>
            <td><code>final boolean</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/ui/Modifier.Node.html#getIsAttached()">isAttached</a>()</code></div>
              <p>Indicates that the node is attached to a <code><a href="/reference/androidx/compose/ui/layout/package-summary.html#Layout(kotlin.Function0,androidx.compose.ui.Modifier,androidx.compose.ui.layout.MeasurePolicy)">androidx.compose.ui.layout.Layout</a></code> which is part of the UI tree.</p>
            </td>
          </tr>
          <tr>
            <td><code>void</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/ui/Modifier.Node.html#onAttach()">onAttach</a>()</code></div>
              <p>Called when the node is attached to a <code><a href="/reference/androidx/compose/ui/layout/package-summary.html#Layout(kotlin.Function0,androidx.compose.ui.Modifier,androidx.compose.ui.layout.MeasurePolicy)">androidx.compose.ui.layout.Layout</a></code> which is part of the UI tree.</p>
            </td>
          </tr>
          <tr>
            <td><code>void</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/ui/Modifier.Node.html#onDetach()">onDetach</a>()</code></div>
              <p>Called when the node is not attached to a <code><a href="/reference/androidx/compose/ui/layout/package-summary.html#Layout(kotlin.Function0,androidx.compose.ui.Modifier,androidx.compose.ui.layout.MeasurePolicy)">androidx.compose.ui.layout.Layout</a></code> which is not a part of the UI tree anymore.</p>
            </td>
          </tr>
          <tr>
            <td><code>void</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/ui/Modifier.Node.html#onReset()">onReset</a>()</code></div>
              <p>Called when the node is about to be moved to a pool of layouts ready to be reused.</p>
            </td>
          </tr>
          <tr>
            <td><code>final void</code></td>
            <td>
              <div><code>@<a href="/reference/androidx/compose/ui/ExperimentalComposeUiApi.html">ExperimentalComposeUiApi</a><br><a href="/reference/androidx/compose/ui/Modifier.Node.html#sideEffect(kotlin.Function0)">sideEffect</a>(@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> Function0&lt;<a href="https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html">Unit</a>&gt;&nbsp;effect)</code></div>
              <p>This can be called to register <code><a href="/reference/androidx/compose/ui/Modifier.Node.html#sideEffect(kotlin.Function0)">effect</a></code> as a function to be executed after all of the changes to the tree are applied.</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="list">
      <h2>Public constructors</h2>
      <div class="api-item"><a name="Node--"></a><a name="node"></a>
        <div class="api-name-block">
          <div>
            <h3 id="Node()">Node</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;<a href="/reference/androidx/compose/ui/Modifier.Node.html#Node()">Node</a>()</pre>
      </div>
    </div>
    <div class="list">
      <h2>Public methods</h2>
      <div class="api-item"><a name="getCoroutineScope--"></a><a name="getcoroutinescope"></a>
        <div class="api-name-block">
          <div>
            <h3 id="getCoroutineScope()">getCoroutineScope</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;final&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/[JVM root]/&lt;Error class: unknown class&gt;.html">&lt;Error class: unknown class&gt;</a>&nbsp;<a href="/reference/androidx/compose/ui/Modifier.Node.html#getCoroutineScope()">getCoroutineScope</a>()</pre>
        <p>A CoroutineScope that can be used to launch tasks that should run while the node is attached.</p>
        <p>The scope is accessible between <code><a href="/reference/androidx/compose/ui/Modifier.Node.html#onAttach()">onAttach</a></code> and <code><a href="/reference/androidx/compose/ui/Modifier.Node.html#onDetach()">onDetach</a></code> calls, and will be cancelled after the node is detached (after <code><a href="/reference/androidx/compose/ui/Modifier.Node.html#onDetach()">onDetach</a></code> returns).</p>
        <pre class="prettyprint lang-kotlin">
import androidx.compose.animation.core.Animatable
import androidx.compose.ui.Modifier

class AnimatedNode : Modifier.Node() {
    val animatable = Animatable(0f)

    override fun onAttach() {
        coroutineScope.launch {
            animatable.animateTo(1f)
        }
    }
}</pre>
        <div class="devsite-table-wrapper">
          <table class="responsive">
            <colgroup>
              <col width="40%">
              <col>
            </colgroup>
            <thead>
              <tr>
                <th colspan="100%">Throws</th>
              </tr>
            </thead>
            <tbody class="list">
              <tr>
                <td><code><a href="https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-illegal-state-exception/index.html">kotlin.IllegalStateException</a></code></td>
                <td>
                  <p>If called while the node is not attached.</p>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="api-item"><a name="getNode--"></a><a name="getnode"></a>
        <div class="api-name-block">
          <div>
            <h3 id="getNode()">getNode</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;final&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/ui/Modifier.Node.html">Modifier.Node</a>&nbsp;<a href="/reference/androidx/compose/ui/Modifier.Node.html#getNode()">getNode</a>()</pre>
        <p>A reference of the <code><a href="/reference/androidx/compose/ui/Modifier.Node.html">Modifier.Node</a></code> that holds this node's position in the node hierarchy. If the node is a delegate of another node, this will point to that node. Otherwise, this will point to itself.</p>
      </div>
      <div class="api-item"><a name="getShouldAutoInvalidate--"></a><a name="getshouldautoinvalidate"></a>
        <div class="api-name-block">
          <div>
            <h3 id="getShouldAutoInvalidate()">getShouldAutoInvalidate</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;boolean&nbsp;<a href="/reference/androidx/compose/ui/Modifier.Node.html#getShouldAutoInvalidate()">getShouldAutoInvalidate</a>()</pre>
        <p>If this property returns <code>true</code>, then nodes will be automatically invalidated after the modifier update completes (For example, if the returned Node is a <code><a href="/reference/androidx/compose/ui/node/DrawModifierNode.html">DrawModifierNode</a></code>, its <code><a href="/reference/androidx/compose/ui/node/package-summary.html#(androidx.compose.ui.node.DrawModifierNode).invalidateDraw()">DrawModifierNode.invalidateDraw</a></code> function will be invoked automatically as part of auto invalidation).</p>
        <p>This is enabled by default, and provides a convenient mechanism to schedule invalidation and apply changes made to the modifier. You may choose to set this to <code>false</code> if your modifier has auto-invalidatable properties that do not frequently require invalidation to improve performance by skipping unnecessary invalidation. If <code>autoInvalidate</code> is set to <code>false</code>, you must call the appropriate invalidate functions manually when the modifier is updated or else the updates may not be reflected in the UI appropriately.</p>
      </div>
      <div class="api-item"><a name="getIsAttached--"></a><a name="getisattached"></a>
        <div class="api-name-block">
          <div>
            <h3 id="getIsAttached()">isAttached</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;final&nbsp;boolean&nbsp;<a href="/reference/androidx/compose/ui/Modifier.Node.html#getIsAttached()">isAttached</a>()</pre>
        <p>Indicates that the node is attached to a <code><a href="/reference/androidx/compose/ui/layout/package-summary.html#Layout(kotlin.Function0,androidx.compose.ui.Modifier,androidx.compose.ui.layout.MeasurePolicy)">androidx.compose.ui.layout.Layout</a></code> which is part of the UI tree. This will get set to true right before <code><a href="/reference/androidx/compose/ui/Modifier.Node.html#onAttach()">onAttach</a></code> is called, and set to false right after <code><a href="/reference/androidx/compose/ui/Modifier.Node.html#onDetach()">onDetach</a></code> is called.</p>
        <div class="devsite-table-wrapper">
          <table class="responsive">
            <colgroup>
              <col width="40%">
              <col>
            </colgroup>
            <thead>
              <tr>
                <th colspan="100%">See also</th>
              </tr>
            </thead>
            <tbody class="list">
              <tr>
                <td><code><a href="/reference/androidx/compose/ui/Modifier.Node.html#onAttach()">onAttach</a></code></td>
                <td></td>
              </tr>
              <tr>
                <td><code><a href="/reference/androidx/compose/ui/Modifier.Node.html#onDetach()">onDetach</a></code></td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="api-item"><a name="onAttach--"></a><a name="onattach"></a>
        <div class="api-name-block">
          <div>
            <h3 id="onAttach()">onAttach</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;void&nbsp;<a href="/reference/androidx/compose/ui/Modifier.Node.html#onAttach()">onAttach</a>()</pre>
        <p>Called when the node is attached to a <code><a href="/reference/androidx/compose/ui/layout/package-summary.html#Layout(kotlin.Function0,androidx.compose.ui.Modifier,androidx.compose.ui.layout.MeasurePolicy)">androidx.compose.ui.layout.Layout</a></code> which is part of the UI tree. When called, <code>node</code> is guaranteed to be non-null. You can call sideEffect, coroutineScope, etc. This is not guaranteed to get called at a time where the rest of the Modifier.Nodes in the hierarchy are &quot;up to date&quot;. For instance, at the time of calling onAttach for this node, another node may be in the tree that will be detached by the time Compose has finished applying changes. As a result, if you need to guarantee that the state of the tree is &quot;final&quot; for this round of changes, you should use the <code><a href="/reference/androidx/compose/ui/Modifier.Node.html#sideEffect(kotlin.Function0)">sideEffect</a></code> API to schedule the calculation to be done at that time.</p>
      </div>
      <div class="api-item"><a name="onDetach--"></a><a name="ondetach"></a>
        <div class="api-name-block">
          <div>
            <h3 id="onDetach()">onDetach</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;void&nbsp;<a href="/reference/androidx/compose/ui/Modifier.Node.html#onDetach()">onDetach</a>()</pre>
        <p>Called when the node is not attached to a <code><a href="/reference/androidx/compose/ui/layout/package-summary.html#Layout(kotlin.Function0,androidx.compose.ui.Modifier,androidx.compose.ui.layout.MeasurePolicy)">androidx.compose.ui.layout.Layout</a></code> which is not a part of the UI tree anymore. Note that the node can be reattached again.</p>
        <p>This should be called right before the node gets removed from the list, so you should still be able to traverse inside of this method. Ideally we would not allow you to trigger side effects here.</p>
      </div>
      <div class="api-item"><a name="onReset--"></a><a name="onreset"></a>
        <div class="api-name-block">
          <div>
            <h3 id="onReset()">onReset</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">public&nbsp;void&nbsp;<a href="/reference/androidx/compose/ui/Modifier.Node.html#onReset()">onReset</a>()</pre>
        <p>Called when the node is about to be moved to a pool of layouts ready to be reused. For example it happens when the node is part of the item of LazyColumn after this item is scrolled out of the viewport. This means this node could be in future reused for a <code><a href="/reference/androidx/compose/ui/layout/package-summary.html#Layout(kotlin.Function0,androidx.compose.ui.Modifier,androidx.compose.ui.layout.MeasurePolicy)">androidx.compose.ui.layout.Layout</a></code> displaying a semantically different content when the list will be populating a new item.</p>
        <p>Use this callback to reset some local item specific state, like &quot;is my component focused&quot;.</p>
        <p>This callback is called while the node is attached. Right after this callback the node will be detached and later reattached when reused.</p>
        <pre class="prettyprint lang-kotlin">
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier

class SelectableNode : Modifier.Node() {
    var selected by mutableStateOf(false)

    override fun onReset() {
        // reset `selected` to the initial value as if the node will be reused for
        // displaying different content it shouldn't be selected straight away.
        selected = false
    }

    // some logic which sets `selected` to true when it is selected
}</pre>
      </div>
      <div class="api-item"><a name="sideEffect-kotlin.Function0-"></a><a name="sideeffect"></a>
        <div class="api-name-block">
          <div>
            <h3 id="sideEffect(kotlin.Function0)">sideEffect</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">@<a href="/reference/androidx/compose/ui/ExperimentalComposeUiApi.html">ExperimentalComposeUiApi</a><br>public&nbsp;final&nbsp;void&nbsp;<a href="/reference/androidx/compose/ui/Modifier.Node.html#sideEffect(kotlin.Function0)">sideEffect</a>(@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> Function0&lt;<a href="https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html">Unit</a>&gt;&nbsp;effect)</pre>
        <p>This can be called to register <code><a href="/reference/androidx/compose/ui/Modifier.Node.html#sideEffect(kotlin.Function0)">effect</a></code> as a function to be executed after all of the changes to the tree are applied.</p>
        <p>This API can only be called if the node <code><a href="/reference/androidx/compose/ui/Modifier.Node.html#isAttached()">isAttached</a></code>.</p>
      </div>
    </div>
  </body>
</html>
