<html devsite="true">
  <head>
    <title>ViewCompositionStrategy</title>
{% setvar book_path %}/reference/androidx/_book.yaml{% endsetvar %}
{% include "_shared/_reference-head-tags.html" %}
  </head>
  <body>
    <div itemscope="" itemtype="http://developers.google.com/ReferenceObject">
      <meta itemprop="name" content="ViewCompositionStrategy">
      <meta itemprop="path" content="androidx.compose.ui.platform">
      <meta itemprop="property" content="installFor(androidx.compose.ui.platform.AbstractComposeView)">
      <meta itemprop="property" content="getDefault()">
      <meta itemprop="property" content="Default()">
      <meta itemprop="language" content="JAVA">
    </div>
    <div id="header-block">
      <div>
        <h1>ViewCompositionStrategy</h1>
      </div>
      <div id="metadata-info-block">
        <div id="source-link"><a href="https://cs.android.com/search?q=file:androidx/compose/ui/platform/ViewCompositionStrategy.android.kt+class:androidx.compose.ui.platform.ViewCompositionStrategy&amp;ss=androidx/platform/frameworks/support" class="external">View Source</a></div>
      </div>
    </div>

{% setvar page_path %}androidx/compose/ui/platform/ViewCompositionStrategy.html{% endsetvar %}
{% setvar can_switch %}1{% endsetvar %}
{% include "reference/_java_switcher2.md" %}
    <p>
      <pre>public interface <a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.html">ViewCompositionStrategy</a></pre>
    </p>
    <div class="devsite-table-wrapper"><devsite-expandable><span class="expand-control jd-sumtable-subclasses">Known direct subclasses
        <div class="showalways" id="subclasses-direct"><a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.DisposeOnDetachedFromWindowOrReleasedFromPool.html">ViewCompositionStrategy.DisposeOnDetachedFromWindowOrReleasedFromPool</a>, <a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.DisposeOnDetachedFromWindow.html">ViewCompositionStrategy.DisposeOnDetachedFromWindow</a>, <a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.DisposeOnLifecycleDestroyed.html">ViewCompositionStrategy.DisposeOnLifecycleDestroyed</a>, <a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed.html">ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed</a></div>
      </span>
      <div id="subclasses-direct-summary">
        <div class="devsite-table-wrapper">
          <table class="responsive">
            <colgroup>
              <col width="40%">
              <col>
            </colgroup>
            <tbody class="list">
              <tr>
                <td><code><a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.DisposeOnDetachedFromWindowOrReleasedFromPool.html">ViewCompositionStrategy.DisposeOnDetachedFromWindowOrReleasedFromPool</a></code></td>
                <td>
                  <p>The composition will be disposed automatically when the view is detached from a window, unless it is part of a <code><a href="/reference/androidx/customview/poolingcontainer/package-summary.html#(android.view.View).isPoolingContainer()">pooling container</a></code>, such as <code>RecyclerView</code>.</p>
                </td>
              </tr>
              <tr>
                <td><code><a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.DisposeOnDetachedFromWindow.html">ViewCompositionStrategy.DisposeOnDetachedFromWindow</a></code></td>
                <td>
                  <p><code><a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.html">ViewCompositionStrategy</a></code> that disposes the composition whenever the view becomes detached from a window.</p>
                </td>
              </tr>
              <tr>
                <td><code><a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.DisposeOnLifecycleDestroyed.html">ViewCompositionStrategy.DisposeOnLifecycleDestroyed</a></code></td>
                <td>
                  <p><code><a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.html">ViewCompositionStrategy</a></code> that disposes the composition when <code><a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.DisposeOnLifecycleDestroyed.html#lifecycle()">lifecycle</a></code> is <code><a href="/reference/androidx/lifecycle/Lifecycle.Event.ON_DESTROY.html">destroyed</a></code>.</p>
                </td>
              </tr>
              <tr>
                <td><code><a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed.html">ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed</a></code></td>
                <td>
                  <p><code><a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.html">ViewCompositionStrategy</a></code> that disposes the composition when the <code><a href="/reference/androidx/lifecycle/LifecycleOwner.html">LifecycleOwner</a></code> returned by <code><a href="/reference/androidx/lifecycle/package-summary.html#(android.view.View).findViewTreeLifecycleOwner()">findViewTreeLifecycleOwner</a></code> of the next window the view is attached to is <code><a href="/reference/androidx/lifecycle/Lifecycle.Event.ON_DESTROY.html">destroyed</a></code>.</p>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
</devsite-expandable>    </div>
    <hr>
    <p>A strategy for managing the underlying Composition of Compose UI <code><a href="https://developer.android.com/reference/android/view/View.html">View</a></code>s such as <code><a href="/reference/androidx/compose/ui/platform/ComposeView.html">ComposeView</a></code> and <code><a href="/reference/androidx/compose/ui/platform/AbstractComposeView.html">AbstractComposeView</a></code>. See <code><a href="/reference/androidx/compose/ui/platform/AbstractComposeView.html#setViewCompositionStrategy(androidx.compose.ui.platform.ViewCompositionStrategy)">AbstractComposeView.setViewCompositionStrategy</a></code>.</p>
    <p>Compose views involve ongoing work and registering the composition with external event sources. These registrations can cause the composition to remain live and ineligible for garbage collection for long after the host View may have been abandoned. These resources and registrations can be released manually at any time by calling <code><a href="/reference/androidx/compose/ui/platform/AbstractComposeView.html#disposeComposition()">AbstractComposeView.disposeComposition</a></code> and a new composition will be created automatically when needed. A <code><a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.html">ViewCompositionStrategy</a></code> defines a strategy for disposing the composition automatically at an appropriate time.</p>
    <p>By default, Compose UI views are configured to <code><a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.Companion.html#Default()">Default</a></code>.</p>
    <h2>Summary</h2>
    <div class="devsite-table-wrapper">
      <table class="responsive">
        <colgroup>
          <col width="40%">
          <col>
        </colgroup>
        <thead>
          <tr>
            <th colspan="100%"><h3>Nested types</h3></th>
          </tr>
        </thead>
        <tbody class="list">
          <tr>
            <td>
              <div><code>public static class <a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.Companion.html">ViewCompositionStrategy.Companion</a></code></div>
              <p>This companion object may be used to define extension factory functions for other strategies to aid in discovery via autocomplete. e.g.: <code>fun ViewCompositionStrategy.Companion.MyStrategy(): MyStrategy</code></p>
            </td>
          </tr>
          <tr>
            <td>
              <div><code>public static class <a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.DisposeOnDetachedFromWindow.html">ViewCompositionStrategy.DisposeOnDetachedFromWindow</a> implements <a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.html">ViewCompositionStrategy</a></code></div>
              <p><code><a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.html">ViewCompositionStrategy</a></code> that disposes the composition whenever the view becomes detached from a window.</p>
            </td>
          </tr>
          <tr>
            <td>
              <div><code>public static class <a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.DisposeOnDetachedFromWindowOrReleasedFromPool.html">ViewCompositionStrategy.DisposeOnDetachedFromWindowOrReleasedFromPool</a> implements <a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.html">ViewCompositionStrategy</a></code></div>
              <p>The composition will be disposed automatically when the view is detached from a window, unless it is part of a <code><a href="/reference/androidx/customview/poolingcontainer/package-summary.html#(android.view.View).isPoolingContainer()">pooling container</a></code>, such as <code>RecyclerView</code>.</p>
            </td>
          </tr>
          <tr>
            <td>
              <div><code>public final class <a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.DisposeOnLifecycleDestroyed.html">ViewCompositionStrategy.DisposeOnLifecycleDestroyed</a> implements <a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.html">ViewCompositionStrategy</a></code></div>
              <p><code><a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.html">ViewCompositionStrategy</a></code> that disposes the composition when <code><a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.DisposeOnLifecycleDestroyed.html#lifecycle()">lifecycle</a></code> is <code><a href="/reference/androidx/lifecycle/Lifecycle.Event.ON_DESTROY.html">destroyed</a></code>.</p>
            </td>
          </tr>
          <tr>
            <td>
              <div><code>public static class <a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed.html">ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed</a> implements <a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.html">ViewCompositionStrategy</a></code></div>
              <p><code><a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.html">ViewCompositionStrategy</a></code> that disposes the composition when the <code><a href="/reference/androidx/lifecycle/LifecycleOwner.html">LifecycleOwner</a></code> returned by <code><a href="/reference/androidx/lifecycle/package-summary.html#(android.view.View).findViewTreeLifecycleOwner()">findViewTreeLifecycleOwner</a></code> of the next window the view is attached to is <code><a href="/reference/androidx/lifecycle/Lifecycle.Event.ON_DESTROY.html">destroyed</a></code>.</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="devsite-table-wrapper">
      <table class="responsive">
        <colgroup>
          <col width="40%">
          <col>
        </colgroup>
        <thead>
          <tr>
            <th colspan="100%"><h3>Public methods</h3></th>
          </tr>
        </thead>
        <tbody class="list">
          <tr>
            <td><code>abstract @<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> Function0&lt;<a href="https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html">Unit</a>&gt;</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.html#installFor(androidx.compose.ui.platform.AbstractComposeView)">installFor</a>(@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/ui/platform/AbstractComposeView.html">AbstractComposeView</a>&nbsp;view)</code></div>
              <p>Install this strategy for <code><a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.html#installFor(androidx.compose.ui.platform.AbstractComposeView)">view</a></code> and return a function that will uninstall it later.</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="list">
      <h2>Public methods</h2>
      <div class="api-item"><a name="installFor-androidx.compose.ui.platform.AbstractComposeView-"></a><a name="installfor"></a>
        <div class="api-name-block">
          <div>
            <h3 id="installFor(androidx.compose.ui.platform.AbstractComposeView)">installFor</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">abstract&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> Function0&lt;<a href="https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html">Unit</a>&gt;&nbsp;<a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.html#installFor(androidx.compose.ui.platform.AbstractComposeView)">installFor</a>(@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> <a href="/reference/androidx/compose/ui/platform/AbstractComposeView.html">AbstractComposeView</a>&nbsp;view)</pre>
        <p>Install this strategy for <code><a href="/reference/androidx/compose/ui/platform/ViewCompositionStrategy.html#installFor(androidx.compose.ui.platform.AbstractComposeView)">view</a></code> and return a function that will uninstall it later. This function should not be called directly; it is called by <code><a href="/reference/androidx/compose/ui/platform/AbstractComposeView.html#setViewCompositionStrategy(androidx.compose.ui.platform.ViewCompositionStrategy)">AbstractComposeView.setViewCompositionStrategy</a></code> after uninstalling the previous strategy.</p>
      </div>
    </div>
  </body>
</html>
