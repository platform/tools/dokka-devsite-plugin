<html devsite="true">
  <head>
    <title>FocusState</title>
{% setvar book_path %}/reference/androidx/_book.yaml{% endsetvar %}
{% include "_shared/_reference-head-tags.html" %}
  </head>
  <body>
    <div itemscope="" itemtype="http://developers.google.com/ReferenceObject">
      <meta itemprop="name" content="FocusState">
      <meta itemprop="path" content="androidx.compose.ui.focus">
      <meta itemprop="property" content="getHasFocus()">
      <meta itemprop="property" content="getIsCaptured()">
      <meta itemprop="property" content="getIsFocused()">
      <meta itemprop="language" content="JAVA">
    </div>
    <div id="header-block">
      <div>
        <h1>FocusState</h1>
      </div>
      <div id="metadata-info-block">
        <div id="source-link"><a href="https://cs.android.com/search?q=file:androidx/compose/ui/focus/FocusState.kt+class:androidx.compose.ui.focus.FocusState&amp;ss=androidx/platform/frameworks/support" class="external">View Source</a></div>
      </div>
    </div>

{% setvar page_path %}androidx/compose/ui/focus/FocusState.html{% endsetvar %}
{% setvar can_switch %}1{% endsetvar %}
{% include "reference/_java_switcher2.md" %}
    <p>
      <pre>public interface <a href="/reference/androidx/compose/ui/focus/FocusState.html">FocusState</a></pre>
    </p>
    <hr>
    <p>The focus state of a <code><a href="/reference/androidx/compose/ui/focus/FocusTargetNode.html">FocusTargetNode</a></code>. Use <code><a href="/reference/androidx/compose/ui/focus/package-summary.html#(androidx.compose.ui.Modifier).onFocusChanged(kotlin.Function1)">onFocusChanged</a></code> or <code><a href="/reference/androidx/compose/ui/focus/package-summary.html#(androidx.compose.ui.Modifier).onFocusEvent(kotlin.Function1)">onFocusEvent</a></code> modifiers to access <code><a href="/reference/androidx/compose/ui/focus/FocusState.html">FocusState</a></code>.</p>
    <pre class="prettyprint lang-kotlin">
import androidx.compose.foundation.border
import androidx.compose.foundation.focusable
import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color.Companion.Black
import androidx.compose.ui.graphics.Color.Companion.Green
import androidx.compose.ui.unit.dp

var color by remember { mutableStateOf(Black) }
Box(
    Modifier
        .border(2.dp, color)
        // The onFocusChanged should be added BEFORE the focusable that is being observed.
        .onFocusChanged { color = if (it.isFocused) Green else Black }
        .focusable()
)</pre>
    <h2>Summary</h2>
    <div class="devsite-table-wrapper">
      <table class="responsive">
        <colgroup>
          <col width="40%">
          <col>
        </colgroup>
        <thead>
          <tr>
            <th colspan="100%"><h3>Public methods</h3></th>
          </tr>
        </thead>
        <tbody class="list">
          <tr>
            <td><code>abstract boolean</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/ui/focus/FocusState.html#getHasFocus()">getHasFocus</a>()</code></div>
              <p>Whether the focus modifier associated with this <code><a href="/reference/androidx/compose/ui/focus/FocusState.html">FocusState</a></code> has a child that is focused.</p>
            </td>
          </tr>
          <tr>
            <td><code>abstract boolean</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/ui/focus/FocusState.html#getIsCaptured()">isCaptured</a>()</code></div>
              <p>Whether focus is captured or not.</p>
            </td>
          </tr>
          <tr>
            <td><code>abstract boolean</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/ui/focus/FocusState.html#getIsFocused()">isFocused</a>()</code></div>
              <p>Whether the component is focused or not.</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="list">
      <h2>Public methods</h2>
      <div class="api-item"><a name="getHasFocus--"></a><a name="gethasfocus"></a>
        <div class="api-name-block">
          <div>
            <h3 id="getHasFocus()">getHasFocus</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">abstract&nbsp;boolean&nbsp;<a href="/reference/androidx/compose/ui/focus/FocusState.html#getHasFocus()">getHasFocus</a>()</pre>
        <p>Whether the focus modifier associated with this <code><a href="/reference/androidx/compose/ui/focus/FocusState.html">FocusState</a></code> has a child that is focused.</p>
        <div class="devsite-table-wrapper">
          <table class="responsive">
            <colgroup>
              <col width="40%">
              <col>
            </colgroup>
            <thead>
              <tr>
                <th colspan="100%">Returns</th>
              </tr>
            </thead>
            <tbody class="list">
              <tr>
                <td><code>boolean</code></td>
                <td>
                  <p>true if a child is focused, false otherwise.</p>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="api-item"><a name="getIsCaptured--"></a><a name="getiscaptured"></a>
        <div class="api-name-block">
          <div>
            <h3 id="getIsCaptured()">isCaptured</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">abstract&nbsp;boolean&nbsp;<a href="/reference/androidx/compose/ui/focus/FocusState.html#getIsCaptured()">isCaptured</a>()</pre>
        <p>Whether focus is captured or not. A focusable component is in a captured state when it wants to hold onto focus. (Eg. when a text field has an invalid phone number). When we are in a captured state, clicking on other focusable items does not clear focus from the currently focused item.</p>
        <p>You can capture focus by calling <code><a href="/reference/androidx/compose/ui/focus/package-summary.html#(androidx.compose.ui.focus.FocusRequesterModifierNode).captureFocus()">focusRequester.captureFocus()</a></code> and free focus by calling <code><a href="/reference/androidx/compose/ui/focus/package-summary.html#(androidx.compose.ui.focus.FocusRequesterModifierNode).freeFocus()">focusRequester.freeFocus()</a></code>.</p>
        <pre class="prettyprint lang-kotlin">
import androidx.compose.foundation.border
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color.Companion.Red
import androidx.compose.ui.graphics.Color.Companion.Transparent
import androidx.compose.ui.unit.dp

val focusRequester = remember { FocusRequester() }
var value by remember { mutableStateOf(&quot;apple&quot;) }
var borderColor by remember { mutableStateOf(Transparent) }
TextField(
    value = value,
    onValueChange = {
        value = it.apply {
            if (length &gt; 5) focusRequester.captureFocus() else focusRequester.freeFocus()
        }
    },
    modifier = Modifier
        .border(2.dp, borderColor)
        .focusRequester(focusRequester)
        .onFocusChanged { borderColor = if (it.isCaptured) Red else Transparent }
)</pre>
        <div class="devsite-table-wrapper">
          <table class="responsive">
            <colgroup>
              <col width="40%">
              <col>
            </colgroup>
            <thead>
              <tr>
                <th colspan="100%">Returns</th>
              </tr>
            </thead>
            <tbody class="list">
              <tr>
                <td><code>boolean</code></td>
                <td>
                  <p>true if focus is captured, false otherwise.</p>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="api-item"><a name="getIsFocused--"></a><a name="getisfocused"></a>
        <div class="api-name-block">
          <div>
            <h3 id="getIsFocused()">isFocused</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">abstract&nbsp;boolean&nbsp;<a href="/reference/androidx/compose/ui/focus/FocusState.html#getIsFocused()">isFocused</a>()</pre>
        <p>Whether the component is focused or not.</p>
        <pre class="prettyprint lang-kotlin">
import androidx.compose.foundation.border
import androidx.compose.foundation.focusable
import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color.Companion.Black
import androidx.compose.ui.graphics.Color.Companion.Green
import androidx.compose.ui.unit.dp

var color by remember { mutableStateOf(Black) }
Box(
    Modifier
        .border(2.dp, color)
        // The onFocusChanged should be added BEFORE the focusable that is being observed.
        .onFocusChanged { color = if (it.isFocused) Green else Black }
        .focusable()
)</pre>
        <div class="devsite-table-wrapper">
          <table class="responsive">
            <colgroup>
              <col width="40%">
              <col>
            </colgroup>
            <thead>
              <tr>
                <th colspan="100%">Returns</th>
              </tr>
            </thead>
            <tbody class="list">
              <tr>
                <td><code>boolean</code></td>
                <td>
                  <p>true if the component is focused, false otherwise.</p>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </body>
</html>
