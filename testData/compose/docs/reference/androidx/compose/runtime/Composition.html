<html devsite="true">
  <head>
    <title>Composition</title>
{% setvar book_path %}/reference/androidx/_book.yaml{% endsetvar %}
{% include "_shared/_reference-head-tags.html" %}
  </head>
  <body>
    <div itemscope="" itemtype="http://developers.google.com/ReferenceObject">
      <meta itemprop="name" content="Composition">
      <meta itemprop="path" content="androidx.compose.runtime">
      <meta itemprop="property" content="dispose()">
      <meta itemprop="property" content="getHasInvalidations()">
      <meta itemprop="property" content="getIsDisposed()">
      <meta itemprop="property" content="setContent(kotlin.Function0)">
      <meta itemprop="language" content="JAVA">
    </div>
    <div id="header-block">
      <div>
        <h1>Composition</h1>
      </div>
      <div id="metadata-info-block">
        <div id="source-link"><a href="https://cs.android.com/search?q=file:androidx/compose/runtime/Composition.kt+class:androidx.compose.runtime.Composition&amp;ss=androidx/platform/frameworks/support" class="external">View Source</a></div>
      </div>
    </div>

{% setvar page_path %}androidx/compose/runtime/Composition.html{% endsetvar %}
{% setvar can_switch %}1{% endsetvar %}
{% include "reference/_java_switcher2.md" %}
    <p>
      <pre>public interface <a href="/reference/androidx/compose/runtime/Composition.html">Composition</a></pre>
    </p>
    <div class="devsite-table-wrapper"><devsite-expandable><span class="expand-control jd-sumtable-subclasses">Known direct subclasses
        <div class="showalways" id="subclasses-direct"><a href="/reference/androidx/compose/runtime/ControlledComposition.html">ControlledComposition</a></div>
      </span>
      <div id="subclasses-direct-summary">
        <div class="devsite-table-wrapper">
          <table class="responsive">
            <colgroup>
              <col width="40%">
              <col>
            </colgroup>
            <tbody class="list">
              <tr>
                <td><code><a href="/reference/androidx/compose/runtime/ControlledComposition.html">ControlledComposition</a></code></td>
                <td>
                  <p>A controlled composition is a <code><a href="/reference/androidx/compose/runtime/Composition.html">Composition</a></code> that can be directly controlled by the caller.</p>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
</devsite-expandable>    </div>
    <hr>
    <p>A composition object is usually constructed for you, and returned from an API that is used to initially compose a UI. For instance, <code><a href="/reference/androidx/compose/runtime/Composition.html#setContent(kotlin.Function0)">setContent</a></code> returns a Composition.</p>
    <p>The <code><a href="/reference/androidx/compose/runtime/Composition.html#dispose()">dispose</a></code> method should be used when you would like to dispose of the UI and the Composition.</p>
    <h2>Summary</h2>
    <div class="devsite-table-wrapper">
      <table class="responsive">
        <colgroup>
          <col width="40%">
          <col>
        </colgroup>
        <thead>
          <tr>
            <th colspan="100%"><h3>Public methods</h3></th>
          </tr>
        </thead>
        <tbody class="list">
          <tr>
            <td><code>abstract void</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/runtime/Composition.html#dispose()">dispose</a>()</code></div>
              <p>Clear the hierarchy that was created from the composition and release resources allocated for composition.</p>
            </td>
          </tr>
          <tr>
            <td><code>abstract boolean</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/runtime/Composition.html#getHasInvalidations()">getHasInvalidations</a>()</code></div>
              <p>Returns true if any pending invalidations have been scheduled.</p>
            </td>
          </tr>
          <tr>
            <td><code>abstract boolean</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/runtime/Composition.html#getIsDisposed()">isDisposed</a>()</code></div>
              <p>True if <code><a href="/reference/androidx/compose/runtime/Composition.html#dispose()">dispose</a></code> has been called.</p>
            </td>
          </tr>
          <tr>
            <td><code>abstract void</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/runtime/Composition.html#setContent(kotlin.Function0)">setContent</a>(@<a href="/reference/androidx/compose/runtime/Composable.html">Composable</a> @<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> Function0&lt;<a href="https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html">Unit</a>&gt;&nbsp;content)</code></div>
              <p>Update the composition with the content described by the <code><a href="/reference/androidx/compose/runtime/Composition.html#setContent(kotlin.Function0)">content</a></code> composable.</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="list">
      <h2>Public methods</h2>
      <div class="api-item"><a name="dispose--"></a><a name="dispose"></a>
        <div class="api-name-block">
          <div>
            <h3 id="dispose()">dispose</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">abstract&nbsp;void&nbsp;<a href="/reference/androidx/compose/runtime/Composition.html#dispose()">dispose</a>()</pre>
        <p>Clear the hierarchy that was created from the composition and release resources allocated for composition. After calling <code><a href="/reference/androidx/compose/runtime/Composition.html#dispose()">dispose</a></code> the composition will no longer be recomposed and calling <code><a href="/reference/androidx/compose/runtime/Composition.html#setContent(kotlin.Function0)">setContent</a></code> will throw an <code><a href="https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-illegal-state-exception/index.html">IllegalStateException</a></code>. Calling <code><a href="/reference/androidx/compose/runtime/Composition.html#dispose()">dispose</a></code> is idempotent, all calls after the first are a no-op.</p>
      </div>
      <div class="api-item"><a name="getHasInvalidations--"></a><a name="gethasinvalidations"></a>
        <div class="api-name-block">
          <div>
            <h3 id="getHasInvalidations()">getHasInvalidations</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">abstract&nbsp;boolean&nbsp;<a href="/reference/androidx/compose/runtime/Composition.html#getHasInvalidations()">getHasInvalidations</a>()</pre>
        <p>Returns true if any pending invalidations have been scheduled. An invalidation is schedule if <code><a href="/reference/androidx/compose/runtime/RecomposeScope.html#invalidate()">RecomposeScope.invalidate</a></code> has been called on any composition scopes create for the composition.</p>
        <p>Modifying <code><a href="/reference/androidx/compose/runtime/MutableState.html#value()">MutableState.value</a></code> of a value produced by <code><a href="/reference/androidx/compose/runtime/package-summary.html#mutableStateOf(kotlin.Any,androidx.compose.runtime.SnapshotMutationPolicy)">mutableStateOf</a></code> will automatically call <code><a href="/reference/androidx/compose/runtime/RecomposeScope.html#invalidate()">RecomposeScope.invalidate</a></code> for any scope that read <code><a href="/reference/androidx/compose/runtime/State.html#value()">State.value</a></code> of the mutable state instance during composition.</p>
        <div class="devsite-table-wrapper">
          <table class="responsive">
            <colgroup>
              <col width="40%">
              <col>
            </colgroup>
            <thead>
              <tr>
                <th colspan="100%">See also</th>
              </tr>
            </thead>
            <tbody class="list">
              <tr>
                <td><code><a href="/reference/androidx/compose/runtime/RecomposeScope.html">RecomposeScope</a></code></td>
                <td></td>
              </tr>
              <tr>
                <td><code><a href="/reference/androidx/compose/runtime/package-summary.html#mutableStateOf(kotlin.Any,androidx.compose.runtime.SnapshotMutationPolicy)">mutableStateOf</a></code></td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="api-item"><a name="getIsDisposed--"></a><a name="getisdisposed"></a>
        <div class="api-name-block">
          <div>
            <h3 id="getIsDisposed()">isDisposed</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">abstract&nbsp;boolean&nbsp;<a href="/reference/androidx/compose/runtime/Composition.html#getIsDisposed()">isDisposed</a>()</pre>
        <p>True if <code><a href="/reference/androidx/compose/runtime/Composition.html#dispose()">dispose</a></code> has been called.</p>
      </div>
      <div class="api-item"><a name="setContent-kotlin.Function0-"></a><a name="setcontent"></a>
        <div class="api-name-block">
          <div>
            <h3 id="setContent(kotlin.Function0)">setContent</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">abstract&nbsp;void&nbsp;<a href="/reference/androidx/compose/runtime/Composition.html#setContent(kotlin.Function0)">setContent</a>(@<a href="/reference/androidx/compose/runtime/Composable.html">Composable</a> @<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> Function0&lt;<a href="https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html">Unit</a>&gt;&nbsp;content)</pre>
        <p>Update the composition with the content described by the <code><a href="/reference/androidx/compose/runtime/Composition.html#setContent(kotlin.Function0)">content</a></code> composable. After this has been called the changes to produce the initial composition has been calculated and applied to the composition.</p>
        <p>Will throw an <code><a href="https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-illegal-state-exception/index.html">IllegalStateException</a></code> if the composition has been disposed.</p>
        <div class="devsite-table-wrapper">
          <table class="responsive">
            <colgroup>
              <col width="40%">
              <col>
            </colgroup>
            <thead>
              <tr>
                <th colspan="100%">Parameters</th>
              </tr>
            </thead>
            <tbody class="list">
              <tr>
                <td><code>@<a href="/reference/androidx/compose/runtime/Composable.html">Composable</a> @<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> Function0&lt;<a href="https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html">Unit</a>&gt;&nbsp;content</code></td>
                <td>
                  <p>A composable function that describes the content of the composition.</p>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="devsite-table-wrapper">
          <table class="responsive">
            <colgroup>
              <col width="40%">
              <col>
            </colgroup>
            <thead>
              <tr>
                <th colspan="100%">Throws</th>
              </tr>
            </thead>
            <tbody class="list">
              <tr>
                <td><code><a href="https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-illegal-state-exception/index.html">kotlin.IllegalStateException</a></code></td>
                <td>
                  <p>thrown in the composition has been <code><a href="/reference/androidx/compose/runtime/Composition.html#dispose()">dispose</a></code>d.</p>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </body>
</html>
