<html devsite="true">
  <head>
    <title>ComposeNodeLifecycleCallback</title>
{% setvar book_path %}/reference/androidx/_book.yaml{% endsetvar %}
{% include "_shared/_reference-head-tags.html" %}
  </head>
  <body>
    <div itemscope="" itemtype="http://developers.google.com/ReferenceObject">
      <meta itemprop="name" content="ComposeNodeLifecycleCallback">
      <meta itemprop="path" content="androidx.compose.runtime">
      <meta itemprop="property" content="onDeactivate()">
      <meta itemprop="property" content="onRelease()">
      <meta itemprop="property" content="onReuse()">
      <meta itemprop="language" content="JAVA">
    </div>
    <div id="header-block">
      <div>
        <h1>ComposeNodeLifecycleCallback</h1>
      </div>
      <div id="metadata-info-block">
        <div id="source-link"><a href="https://cs.android.com/search?q=file:androidx/compose/runtime/ComposeNodeLifecycleCallback.kt+class:androidx.compose.runtime.ComposeNodeLifecycleCallback&amp;ss=androidx/platform/frameworks/support" class="external">View Source</a></div>
      </div>
    </div>

{% setvar page_path %}androidx/compose/runtime/ComposeNodeLifecycleCallback.html{% endsetvar %}
{% setvar can_switch %}1{% endsetvar %}
{% include "reference/_java_switcher2.md" %}
    <p>
      <pre>public interface <a href="/reference/androidx/compose/runtime/ComposeNodeLifecycleCallback.html">ComposeNodeLifecycleCallback</a></pre>
    </p>
    <hr>
    <p>Observes lifecycle of the node emitted with <code><a href="/reference/androidx/compose/runtime/package-summary.html#ReusableComposeNode(kotlin.Function0,kotlin.Function1)">ReusableComposeNode</a></code> or <code><a href="/reference/androidx/compose/runtime/package-summary.html#ComposeNode(kotlin.Function0,kotlin.Function1)">ComposeNode</a></code> inside <code><a href="/reference/androidx/compose/runtime/package-summary.html#ReusableContentHost(kotlin.Boolean,kotlin.Function0)">ReusableContentHost</a></code> and <code><a href="/reference/androidx/compose/runtime/package-summary.html#ReusableContent(kotlin.Any,kotlin.Function0)">ReusableContent</a></code>.</p>
    <p>The <code><a href="/reference/androidx/compose/runtime/package-summary.html#ReusableContentHost(kotlin.Boolean,kotlin.Function0)">ReusableContentHost</a></code> introduces the concept of reusing (or recycling) nodes, as well as deactivating parts of composition, while keeping the nodes around to reuse common structures in the next iteration. In this state, <code><a href="/reference/androidx/compose/runtime/RememberObserver.html">RememberObserver</a></code> is not sufficient to track lifetime of data associated with reused node, as deactivated or reused parts of composition is disposed.</p>
    <p>These callbacks track intermediate states of the node in reusable groups for managing data contained inside reusable nodes or associated with them (e.g. subcomposition).</p>
    <p>Important: the runtime only supports node implementation of this interface.</p>
    <h2>Summary</h2>
    <div class="devsite-table-wrapper">
      <table class="responsive">
        <colgroup>
          <col width="40%">
          <col>
        </colgroup>
        <thead>
          <tr>
            <th colspan="100%"><h3>Public methods</h3></th>
          </tr>
        </thead>
        <tbody class="list">
          <tr>
            <td><code>abstract void</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/runtime/ComposeNodeLifecycleCallback.html#onDeactivate()">onDeactivate</a>()</code></div>
              <p>Invoked when the group containing the node was deactivated.</p>
            </td>
          </tr>
          <tr>
            <td><code>abstract void</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/runtime/ComposeNodeLifecycleCallback.html#onRelease()">onRelease</a>()</code></div>
              <p>Invoked when the node exits the composition entirely and won't be reused again.</p>
            </td>
          </tr>
          <tr>
            <td><code>abstract void</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/runtime/ComposeNodeLifecycleCallback.html#onReuse()">onReuse</a>()</code></div>
              <p>Invoked when the node was reused in the composition.</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="list">
      <h2>Public methods</h2>
      <div class="api-item"><a name="onDeactivate--"></a><a name="ondeactivate"></a>
        <div class="api-name-block">
          <div>
            <h3 id="onDeactivate()">onDeactivate</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">abstract&nbsp;void&nbsp;<a href="/reference/androidx/compose/runtime/ComposeNodeLifecycleCallback.html#onDeactivate()">onDeactivate</a>()</pre>
        <p>Invoked when the group containing the node was deactivated. This happens when the content of <code><a href="/reference/androidx/compose/runtime/package-summary.html#ReusableContentHost(kotlin.Boolean,kotlin.Function0)">ReusableContentHost</a></code> is deactivated.</p>
        <p>The node will not be reused in this recompose cycle, but might be reused or released in the future. Consumers might use this callback to release expensive resources or stop continuous process that was dependent on the node being used in composition.</p>
        <p>If the node is reused, <code><a href="/reference/androidx/compose/runtime/ComposeNodeLifecycleCallback.html#onReuse()">onReuse</a></code> will be called again to prepare the node for reuse. Similarly, <code><a href="/reference/androidx/compose/runtime/ComposeNodeLifecycleCallback.html#onRelease()">onRelease</a></code> will indicate that deactivated node will never be reused again.</p>
      </div>
      <div class="api-item"><a name="onRelease--"></a><a name="onrelease"></a>
        <div class="api-name-block">
          <div>
            <h3 id="onRelease()">onRelease</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">abstract&nbsp;void&nbsp;<a href="/reference/androidx/compose/runtime/ComposeNodeLifecycleCallback.html#onRelease()">onRelease</a>()</pre>
        <p>Invoked when the node exits the composition entirely and won't be reused again. All intermediate data related to the node can be safely disposed.</p>
      </div>
      <div class="api-item"><a name="onReuse--"></a><a name="onreuse"></a>
        <div class="api-name-block">
          <div>
            <h3 id="onReuse()">onReuse</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">abstract&nbsp;void&nbsp;<a href="/reference/androidx/compose/runtime/ComposeNodeLifecycleCallback.html#onReuse()">onReuse</a>()</pre>
        <p>Invoked when the node was reused in the composition. Consumers might use this callback to reset data associated with the previous content, as it is no longer valid.</p>
      </div>
    </div>
  </body>
</html>
