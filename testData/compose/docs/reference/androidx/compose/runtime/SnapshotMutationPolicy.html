<html devsite="true">
  <head>
    <title>SnapshotMutationPolicy</title>
{% setvar book_path %}/reference/androidx/_book.yaml{% endsetvar %}
{% include "_shared/_reference-head-tags.html" %}
  </head>
  <body>
    <div itemscope="" itemtype="http://developers.google.com/ReferenceObject">
      <meta itemprop="name" content="SnapshotMutationPolicy">
      <meta itemprop="path" content="androidx.compose.runtime">
      <meta itemprop="property" content="equivalent(kotlin.Any,kotlin.Any)">
      <meta itemprop="property" content="merge(kotlin.Any,kotlin.Any,kotlin.Any)">
      <meta itemprop="language" content="JAVA">
    </div>
    <div id="header-block">
      <div>
        <h1>SnapshotMutationPolicy</h1>
      </div>
      <div id="metadata-info-block">
        <div id="source-link"><a href="https://cs.android.com/search?q=file:androidx/compose/runtime/SnapshotMutationPolicy.kt+class:androidx.compose.runtime.SnapshotMutationPolicy&amp;ss=androidx/platform/frameworks/support" class="external">View Source</a></div>
      </div>
    </div>

{% setvar page_path %}androidx/compose/runtime/SnapshotMutationPolicy.html{% endsetvar %}
{% setvar can_switch %}1{% endsetvar %}
{% include "reference/_java_switcher2.md" %}
    <p>
      <pre>public interface <a href="/reference/androidx/compose/runtime/SnapshotMutationPolicy.html">SnapshotMutationPolicy</a>&lt;T&nbsp;extends&nbsp;<a href="https://developer.android.com/reference/java/lang/Object.html">Object</a>&gt;</pre>
    </p>
    <hr>
    <p>A policy to control how the result of <code><a href="/reference/androidx/compose/runtime/package-summary.html#mutableStateOf(kotlin.Any,androidx.compose.runtime.SnapshotMutationPolicy)">mutableStateOf</a></code> report and merge changes to the state object.</p>
    <p>A mutation policy can be passed as an parameter to <code><a href="/reference/androidx/compose/runtime/package-summary.html#mutableStateOf(kotlin.Any,androidx.compose.runtime.SnapshotMutationPolicy)">mutableStateOf</a></code>, and <code><a href="/reference/androidx/compose/runtime/package-summary.html#compositionLocalOf(androidx.compose.runtime.SnapshotMutationPolicy,kotlin.Function0)">compositionLocalOf</a></code>.</p>
    <p>Typically, one of the stock policies should be used such as <code><a href="/reference/androidx/compose/runtime/package-summary.html#referentialEqualityPolicy()">referentialEqualityPolicy</a></code>, <code><a href="/reference/androidx/compose/runtime/package-summary.html#structuralEqualityPolicy()">structuralEqualityPolicy</a></code>, or <code><a href="/reference/androidx/compose/runtime/package-summary.html#neverEqualPolicy()">neverEqualPolicy</a></code>. However, a custom mutation policy can be created by implementing this interface, such as a counter policy,</p>
    <pre class="prettyprint lang-kotlin">
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.SnapshotMutationPolicy
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.snapshots.Snapshot

/**
 * A policy that treats an `MutableState&lt;Int&gt;` as a counter. Changing the value to the same
 * integer value will not be considered a change. When snapshots are applied the changes made by
 * the applying snapshot are added together with changes of other snapshots. Changes to a
 * [MutableState] with a counterPolicy will never cause an apply conflict.
 *
 * As the name implies, this is useful when counting things, such as tracking the amount of
 * a resource consumed or produced while in a snapshot. For example, if snapshot A produces 10
 * things and snapshot B produces 20 things, the result of applying both A and B should be that
 * 30 things were produced.
 */
fun counterPolicy(): SnapshotMutationPolicy&lt;Int&gt; = object : SnapshotMutationPolicy&lt;Int&gt; {
    override fun equivalent(a: Int, b: Int): Boolean = a == b
    override fun merge(previous: Int, current: Int, applied: Int) =
        current + (applied - previous)
}

val state = mutableStateOf(0, counterPolicy())
val snapshot1 = Snapshot.takeMutableSnapshot()
val snapshot2 = Snapshot.takeMutableSnapshot()
try {
    snapshot1.enter { state.value += 10 }
    snapshot2.enter { state.value += 20 }
    snapshot1.apply().check()
    snapshot2.apply().check()
} finally {
    snapshot1.dispose()
    snapshot2.dispose()
}

// State is now equals 30 as the changes made in the snapshots are added together.</pre>
    <h2>Summary</h2>
    <div class="devsite-table-wrapper">
      <table class="responsive">
        <colgroup>
          <col width="40%">
          <col>
        </colgroup>
        <thead>
          <tr>
            <th colspan="100%"><h3>Public methods</h3></th>
          </tr>
        </thead>
        <tbody class="list">
          <tr>
            <td><code>abstract boolean</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/runtime/SnapshotMutationPolicy.html#equivalent(kotlin.Any,kotlin.Any)">equivalent</a>(@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T&nbsp;a,&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T&nbsp;b)</code></div>
              <p>Determine if setting a state value's are equivalent and should be treated as equal.</p>
            </td>
          </tr>
          <tr>
            <td><code>default T</code></td>
            <td>
              <div><code><a href="/reference/androidx/compose/runtime/SnapshotMutationPolicy.html#merge(kotlin.Any,kotlin.Any,kotlin.Any)">merge</a>(@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T&nbsp;previous,&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T&nbsp;current,&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T&nbsp;applied)</code></div>
              <p>Merge conflicting changes in snapshots.</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="list">
      <h2>Public methods</h2>
      <div class="api-item"><a name="equivalent(kotlin.Any, kotlin.Any)"></a><a name="equivalent-kotlin.Any-kotlin.Any-"></a><a name="equivalent"></a>
        <div class="api-name-block">
          <div>
            <h3 id="equivalent(kotlin.Any,kotlin.Any)">equivalent</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">abstract&nbsp;boolean&nbsp;<a href="/reference/androidx/compose/runtime/SnapshotMutationPolicy.html#equivalent(kotlin.Any,kotlin.Any)">equivalent</a>(@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T&nbsp;a,&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T&nbsp;b)</pre>
        <p>Determine if setting a state value's are equivalent and should be treated as equal. If <code><a href="/reference/androidx/compose/runtime/SnapshotMutationPolicy.html#equivalent(kotlin.Any,kotlin.Any)">equivalent</a></code> returns <code>true</code> the new value is not considered a change.</p>
      </div>
      <div class="api-item"><a name="merge(kotlin.Any, kotlin.Any, kotlin.Any)"></a><a name="merge-kotlin.Any-kotlin.Any-kotlin.Any-"></a><a name="merge"></a>
        <div class="api-name-block">
          <div>
            <h3 id="merge(kotlin.Any,kotlin.Any,kotlin.Any)">merge</h3>
          </div>
        </div>
        <pre class="api-signature no-pretty-print">default&nbsp;T&nbsp;<a href="/reference/androidx/compose/runtime/SnapshotMutationPolicy.html#merge(kotlin.Any,kotlin.Any,kotlin.Any)">merge</a>(@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T&nbsp;previous,&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T&nbsp;current,&nbsp;@<a href="/reference/androidx/annotation/NonNull.html">NonNull</a> T&nbsp;applied)</pre>
        <p>Merge conflicting changes in snapshots. This is only called if <code><a href="/reference/androidx/compose/runtime/SnapshotMutationPolicy.html#merge(kotlin.Any,kotlin.Any,kotlin.Any)">current</a></code> and <code><a href="/reference/androidx/compose/runtime/SnapshotMutationPolicy.html#merge(kotlin.Any,kotlin.Any,kotlin.Any)">applied</a></code> are not <code><a href="/reference/androidx/compose/runtime/SnapshotMutationPolicy.html#equivalent(kotlin.Any,kotlin.Any)">equivalent</a></code>. If a valid merged value can be calculated then it should be returned.</p>
        <p>For example, if the state object holds an immutable data class with multiple fields, and <code><a href="/reference/androidx/compose/runtime/SnapshotMutationPolicy.html#merge(kotlin.Any,kotlin.Any,kotlin.Any)">applied</a></code> has changed fields that are unmodified by <code><a href="/reference/androidx/compose/runtime/SnapshotMutationPolicy.html#merge(kotlin.Any,kotlin.Any,kotlin.Any)">current</a></code> it might be valid to return a new copy of the data class that combines that changes from both <code><a href="/reference/androidx/compose/runtime/SnapshotMutationPolicy.html#merge(kotlin.Any,kotlin.Any,kotlin.Any)">current</a></code> and <code><a href="/reference/androidx/compose/runtime/SnapshotMutationPolicy.html#merge(kotlin.Any,kotlin.Any,kotlin.Any)">applied</a></code> allowing a snapshot to apply that would have otherwise failed.</p>
        <pre class="prettyprint lang-kotlin">
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.SnapshotMutationPolicy
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.snapshots.Snapshot

/**
 * A policy that treats an `MutableState&lt;Int&gt;` as a counter. Changing the value to the same
 * integer value will not be considered a change. When snapshots are applied the changes made by
 * the applying snapshot are added together with changes of other snapshots. Changes to a
 * [MutableState] with a counterPolicy will never cause an apply conflict.
 *
 * As the name implies, this is useful when counting things, such as tracking the amount of
 * a resource consumed or produced while in a snapshot. For example, if snapshot A produces 10
 * things and snapshot B produces 20 things, the result of applying both A and B should be that
 * 30 things were produced.
 */
fun counterPolicy(): SnapshotMutationPolicy&lt;Int&gt; = object : SnapshotMutationPolicy&lt;Int&gt; {
    override fun equivalent(a: Int, b: Int): Boolean = a == b
    override fun merge(previous: Int, current: Int, applied: Int) =
        current + (applied - previous)
}

val state = mutableStateOf(0, counterPolicy())
val snapshot1 = Snapshot.takeMutableSnapshot()
val snapshot2 = Snapshot.takeMutableSnapshot()
try {
    snapshot1.enter { state.value += 10 }
    snapshot2.enter { state.value += 20 }
    snapshot1.apply().check()
    snapshot2.apply().check()
} finally {
    snapshot1.dispose()
    snapshot2.dispose()
}

// State is now equals 30 as the changes made in the snapshots are added together.</pre>
      </div>
    </div>
  </body>
</html>
