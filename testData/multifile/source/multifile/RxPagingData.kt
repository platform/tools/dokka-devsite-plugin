/*
 * Copyright 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:JvmName("PagingRx")
@file:JvmMultifileClass
@file:Suppress("NotARealLintIssue")

package multifile

import androidx.annotation.CheckResult
import androidx.paging.PagingData
import androidx.paging.filter
import androidx.paging.flatMap
import androidx.paging.insertSeparators
import androidx.paging.map
import io.reactivex.Maybe
import io.reactivex.Single
import kotlinx.coroutines.rx2.await

/**
 * Returns a [PagingData] containing only elements matching the given [predicate].
 */
@JvmName("filter")
@CheckResult
fun <T : Any> PagingData<T>.filterAsync(
    predicate: (T) -> Single<Boolean>
): PagingData<T> = filter { predicate(it).await() }
